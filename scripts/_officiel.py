import json, re, sklearn_crfsuite


# from hipe_utils import ConllReader
# from features import *


# train_file='../data/training-v0.9/fr/HIPE-data-v0.9-train-fr.tsv'
# dev_file='../data/training-v0.9/fr/HIPE-data-v0.9-dev-fr.tsv'
train_file = (
    "../../../Documents/hipe2020/data/training-v0.9/fr/HIPE-data-v0.9-train-fr.tsv"
)
dev_file = "../../../Documents/hipe2020/data/training-v0.9/fr/HIPE-data-v0.9-dev-fr.tsv"

train_file = ConllReader(train_file)
dev_file = ConllReader(dev_file)
train_file.drop_nonwords()
dev_file.drop_nonwords()


train_sents, test_sents = [], []

col_name = "NE-COARSE-LIT"

for doc in train_file.json[:1000]:
    _ls = []
    # for tok, pos in zip(doc['lis_tokens'], treetaggerwrapper.make_tags(tagger.tag_text([t_['TOKEN'] for t_ in doc['lis_tokens']]))):
    # print(tok['TOKEN'], pos)
    # _ls.append( (tok['TOKEN'], pos.pos[:3], tok[col_name]) )
    for tok in doc["lis_tokens"]:
        _ls.append((tok["TOKEN"], "___", tok[col_name]))
    train_sents.append(_ls)
for doc in dev_file.json:
    _ls = []
    # for tok, pos in zip(doc['lis_tokens'], treetaggerwrapper.make_tags(tagger.tag_text([t_['TOKEN'] for t_ in doc['lis_tokens']]))):
    # print(tok['TOKEN'], pos)
    # _ls.append( (tok['TOKEN'], pos.pos[:3], tok[col_name]) )
    for tok in doc["lis_tokens"]:
        _ls.append((tok["TOKEN"], "___", tok[col_name]))
    test_sents.append(_ls)

perso_X_train = [sent2features(s) for s in train_sents]
perso_y_train = [sent2labels(s) for s in train_sents]

perso_X_test = [sent2features(s) for s in test_sents]
perso_y_test = [sent2labels(s) for s in test_sents]

perso_crf = sklearn_crfsuite.CRF(
    # algorithm="lbfgs", c1=0.1, c2=0.1, max_iterations=100, all_possible_transitions=True
    algorithm="lbfgs",
    c1=0.10781312027504897,
    c2=0.005004815370857285,
    max_iterations=558,
    all_possible_transitions=False,
)

perso_crf.fit(perso_X_train, perso_y_train)

# labels = list(crf.classes_)
# labels.remove('O')

perso_y_pred = perso_crf.predict(perso_X_test)


perso_file_pred = "_perso.tsv"

with open(perso_file_pred, "w") as f:
    print("\t".join(train_file.attrs), file=f)
    for doc, pred in zip(dev_file.json, perso_y_pred):
        print(
            f"# document_id" + "\t" * 9 + "\n" + "# segment_iiif_link" + "\t" * 9,
            file=f,
        )  # : {doc['document_id']}
        for i, (tok, pre) in enumerate(zip(doc["lis_tokens"], pred)):
            if tok == "#":
                # on the official results the '#' isn't shown...
                continue
            print(tok["TOKEN"], pre, "O", "\t" * 6, sep="\t", file=f)
            if "EndOfLine" in tok["MISC"] and i != len(doc["lis_tokens"]) - 1:
                print("# segment_iiif_link" + "\t" * 9, file=f)
