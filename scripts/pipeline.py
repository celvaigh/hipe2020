#!/bin/python3
"""Pipeline to compute NER & NEL back and forth
following the PEP 8 syntax (https://www.python.org/dev/peps/pep-0008/)
code regularly blacked (https://github.com/psf/black)
"""
import logging
import numpy as np
import pickle
import os
import warnings

from tqdm import tqdm
from pprint import pprint
from termcolor import colored

logging.basicConfig(
    level=logging.INFO,
    format="[%(asctime)s] - [%(name)s] - [%(levelname)s] - %(message)s",
    handlers=[logging.FileHandler(__file__ + ".log"), logging.StreamHandler()],
)

logger = logging.getLogger(__name__)


from hipe_utils import ConllReader
from NER_crf import Hipe_CRF

# from CEL import Linker
from NELv2 import Linker
from params import get_parser


class Pipeline(Hipe_CRF, Linker):
    def __init__(self, args_dict, skip_linker=False):
        # .__mro__ : (__main__.Pipeline, NER_crf.Hipe_CRF, NELv2.Linker, object)

        np.random.seed(args_dict.seed)
        # NER PART
        super(Pipeline, self).__init__(args_dict)
        # Hipe_CRF.__init__(self,args_dict)

        # NEL PART
        if not skip_linker:
            Linker.__init__(self, args_dict)

        # adding files to boost the score:
        self.train_file_conll.add_file(
            args_dict.path + "data/release-v01/fr/HIPE-data-v01-sample-fr.tsv"
        )
        self.train_file_conll.add_file(
            args_dict.path + "data/training-v0.9/en/HIPE-data-v0.9-dev-en.tsv"
        )

    def loop(self, n):
        """performs the NER and NEL several times"""

        for i in range(n):
            logger.debug(f"looping n{i}")
            logger.debug("performing NER")
            if not i:
                # 1st time: do not use the NEL results
                self.train_predict()
            else:
                self.train_predict(middle_col=True)

            logger.debug("performing NEL base on NER results")
            self.link_conll(pipe.dev_file_conll)

            logger.debug("getting the results")
            self.get_results()
            str_ = f'RESULTS {i}\tstrict_F1_micro: {pipe.results["NE-COARSE-LIT"]["ALL"]["strict"]["F1_micro"]}\tent_type_F1_micro: {pipe.results["NE-COARSE-LIT"]["ALL"]["ent_type"]["F1_micro"]}'
            logging.info(str_)
            print(str_, file=open("_res.txt", "a"))

    def update_lis_token_with_NEL_results(self):
        """update the self.dev_file_conll.json['lis_tokens'] w/ the NEL values:
        add a 'NEL-result' key to the tokens """
        for doc in self.dev_file_conll:
            for tok in doc["lis_tokens"]:
                # lab_=self.get_hipe_label(tok['NEL-LIT'])
                # if lab_ !='None':
                # tok['NEL-result'] = lab_
                tok["NEL-result"] = self.get_hipe_label(tok["NEL-LIT"])
                # if lab_ !='None':
                # tok['NEL-result'] = lab_

    def update_NE_type_with_NEL_result(
        self, ne_colname="NE-COARSE-LIT", nel_colname="NEL-LIT"
    ):
        """update the colname with the results of the NEL (organization, PERS, ..."""
        for doc in self.dev_file_conll:
            for tok in doc["lis_tokens"]:
                if tok["IS_WORD"] and tok[nel_colname] not in ("_", "NIL", "NA"):
                    lab_ = self.get_hipe_label(tok[nel_colname])
                    if lab_.upper() not in ("NONE", ""):
                        if self.verbose:
                            print(
                                f'updating "{tok["TOKEN"]}":\t"{tok[ne_colname]}" by "{tok[ne_colname].split("-")[0] + "-" + lab_.upper()}"'
                            )
                        tok[ne_colname] = (
                            tok[ne_colname].split("-")[0] + "-" + lab_.upper()
                        )

    def ouput_NER_NEL_difference(self, out_file=None, beautiful_colors=True):
        """output the differences with original files after computing NER NEL
            GDL-1988-11-22-a-i0054
            num	TOKEN	NER-predictions	NELQ	NEL-LABEL	NE-OFFICIEL	NELQ-Offi	difference
            13	AFP	B-ORG	Q40464	org	B-org
            18	géant	O	Q3294930	org	B-org	XXXXXXXXXXXXXXXX
            19	américain	O	Q3294930	org	I-org	XXXXXXXXXXXXXXXX
            20	du	O	Q3294930	org	I-org	XXXXXXXXXXXXXXXX
            21	tabac	O	Q3294930	org	I-org	XXXXXXXXXXXXXXXX
           """
        str_ = ""
        for doc, doc_offi in zip(
            self.dev_file_conll.json, self.original_dev_file_conll.json
        ):
            if beautiful_colors:
                str_ += "\n" + colored(doc["document_id"], "green", "on_blue") + "\n"
            else:
                str_ += "\n" + doc["document_id"] + "\n"
            str_ += "num\tTOKEN\tNER-pred\tNELQ\tNEL-LABEL\tNE-OFFI\tNELQ-Offi\tdifference\n"
            for i, (tok, tok_offi) in enumerate(
                zip(doc["lis_tokens"], doc_offi["lis_tokens"])
            ):
                if tok["NEL-LIT"] in ("NIL", "NA", "_"):
                    continue
                str_ += (
                    "\t".join(
                        [
                            str(i),
                            tok["TOKEN"],
                            tok["NE-COARSE-LIT"],
                            tok["NEL-LIT"],
                            self.get_hipe_label(tok["NEL-LIT"]),
                            tok_offi["NE-COARSE-LIT"],
                        ]
                    )
                    + "\t"
                )
                if (
                    tok["NE-COARSE-LIT"][-3:]
                    != self.get_hipe_label(tok["NEL-LIT"]).upper()[-3:]
                ):
                    if beautiful_colors:
                        str_ += colored(
                            " NEL RESULT DIFFERENT FROM PRED ", "yellow", "on_red"
                        )
                # print()
                #    print(tok['NEL-LIT'].upper())
                if tok["NE-COARSE-LIT"] != tok_offi["NE-COARSE-LIT"].upper():
                    if beautiful_colors:
                        str_ += colored(
                            " NER RESULT DIFFRENT FROM OFFI ", "yellow", "on_green"
                        )
                if tok_offi["NE-COARSE-LIT"][-3:] == self.get_hipe_label(
                    tok["NEL-LIT"]
                ):
                    str_ += colored(" NEL IS WRIGHT ", "yellow", "on_blue")
                str_ += "\n"
        if out_file:
            print(str_, file=open(out_file, "w"))
        else:
            print(str_)

    def compare_2_files_with_beautiful_colors(self, col_name="NE-COARSE-LIT"):
        """compare the result of the dev file to the original file"""
        for doc_pred, doc_offi in zip(
            self.dev_file_conll, self.original_dev_file_conll
        ):
            print(colored(doc_pred["document_id"], "yellow", "on_red"))
            print(
                colored(
                    "TOKEN\tNE-PRED\tNE-OFFICIEL\tNEL-PRED\tNEL-OFFI\tLOCAL-KB",
                    "yellow",
                )
            )
            for tok_pred, tok_offi in zip(
                doc_pred["lis_tokens"], doc_offi["lis_tokens"]
            ):
                if (
                    tok_pred[col_name] != tok_offi[col_name].upper()
                    and tok_offi["IS_WORD"]
                ):
                    print(
                        colored(
                            tok_pred["TOKEN"]
                            + "\t"
                            + tok_pred[col_name]
                            + "\t"
                            + tok_offi[col_name]
                            + "\t"
                            + tok_pred["NEL-LIT"]
                            + "\t"
                            + tok_offi["NEL-LIT"]
                            + "\t"
                            + str(self.dict_Q_mention[tok_pred["NEL-LIT"]]),
                            "yellow",
                            "on_blue",
                        )
                    )
                elif not tok_offi["IS_WORD"]:
                    print(tok_offi["TOKEN"])
                else:
                    print(
                        tok_pred["TOKEN"]
                        + "\t"
                        + tok_pred[col_name]
                        + "\t"
                        + tok_offi[col_name]
                        + "\t"
                        + tok_pred["NEL-LIT"]
                        + "\t"
                        + tok_offi["NEL-LIT"]
                        + "\t"
                        + str(self.dict_Q_mention[tok_pred["NEL-LIT"]])
                    )
            print()


def test_NER_NEL_NER():
    """function to test a NER, a NEL based on the NER and a NER based on the NEL"""
    parser = get_parser()
    args_dict, unknown = parser.parse_known_args()
    with Pipeline(args_dict) as pipe:
        # NER initial
        pipe.train_predict()
        pipe.get_results()
        res_NER_init = (
            pipe.results["NE-COARSE-LIT"]["ALL"]["strict"]["F1_micro"],
            pipe.results["NE-COARSE-LIT"]["ALL"]["ent_type"]["F1_micro"],
        )

        #  NEL
        pipe.link_conll(pipe.dev_file_conll)

        # NER again
        pipe.train_predict(middle_col=True)
        pipe.get_results()
        res_NER_2nd = (
            pipe.results["NE-COARSE-LIT"]["ALL"]["strict"]["F1_micro"],
            pipe.results["NE-COARSE-LIT"]["ALL"]["ent_type"]["F1_micro"],
        )
        return {"results_1_initia": res_NER_init, "results_2nd_turn": res_NER_2nd}


def test_NER_NEL_corr():
    """perform NER, NEL and replace the NER result by the NEL type"""
    parser = get_parser()
    args_dict, unknown = parser.parse_known_args()

    with Pipeline(args_dict) as pipe:
        # NER initial
        pipe.train_predict()
        pipe.get_results()
        res_NER_init = (
            pipe.results["NE-COARSE-LIT"]["ALL"]["strict"]["F1_micro"],
            pipe.results["NE-COARSE-LIT"]["ALL"]["ent_type"]["F1_micro"],
        )

        #  NEL
        pipe.link_conll(pipe.dev_file_conll)

        # NER again
        pipe.update_NE_type_with_NEL_result()

        pipe.get_results()
        res_NER_2nd = (
            pipe.results["NE-COARSE-LIT"]["ALL"]["strict"]["F1_micro"],
            pipe.results["NE-COARSE-LIT"]["ALL"]["ent_type"]["F1_micro"],
        )
        return {"results_1_initia": res_NER_init, "results_2nd_turn": res_NER_2nd}


if __name__ == "__main__":
    logger.info("_" * 50)
    logger.info(__file__)

    parser = get_parser()
    args_dict, unknown = parser.parse_known_args()
    for arg in unknown:
        warnings.warn(f"unknown parameter:\t" + arg)

    opts = vars(args_dict)
    print("------------ Options -------------")
    for k, v in sorted(opts.items()):
        print("%s: %s" % (str(k), str(v)))  # logging.info("%s: %s" % (str(k), str(v)))
    print("-----------------------------------")

    pipe = Pipeline(args_dict)

    logging.debug(pipe)

    pipe.train_predict()

    pipe.link_conll(pipe.dev_file_conll)
    try:
        pipe.dev_file_conll.to_conll(os.path.join(pipe.path, pipe.output_crf))
    except Exception as e:
        print(e)
        pipe.dev_file_conll.to_conll(os.path.join("..", pipe.output_crf))
    pipe.save()
