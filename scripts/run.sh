#!/bin/bash



root=/mnt/nfs/nas4/cicoda_tmp/cicoda_tmp/HIPE/hipe2020/
embed_file=/mnt/nfs/nas4/cicoda_tmp/cicoda_tmp/word2vec/trunk/models/skipgram.s100.w5.enwikianchor1G.bin
train=data/training-v1.0/fr/HIPE-data-v1.0-train-fr.tsv
test=results/irisa_bundle2_fr_2.tsv
output=results/fr/irisa-nel-mlp-ner_bundle2_fr_1.tsv
script=$root/scripts/pipeline.py
# for c in VC #REG GB MLP QDA KNN ADAB RF SVM 
# do
#     python3 $script --path $root --train $train --test $test --embed_file $embed_file  --output $output --classifier $c
#     python3 CLEF-HIPE-2020-scorer/clef_evaluation.py -r ../$test -p ../$output -t nel
#     python3 out_res.py  _.json
# done
# for f in "" 0 1 0,1 0,1,7
# do
for c in REG GB MLP QDA KNN ADAB RF SVM
do
    python3 $script --path $root --train $train --test $test --embed_file $embed_file  --output $output --classifier $c
    python3 $root/scripts/CLEF-HIPE-2020-scorer/clef_evaluation.py -r $root/$test -p $root/$output -t nel
    python3 $root/scripts/out_res.py  $root/scripts/_.json
done
#done
