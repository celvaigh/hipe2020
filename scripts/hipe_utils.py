"""classes and functions used as utilities
"""

import json
import random
import re
import requests

# from googletrans import Translator
from tqdm import tqdm

# from spellchecker import SpellChecker

# spell = SpellChecker(language='fr')


def deeppav_2_hipe_coarse(anno: str) -> str:
    """return a hipe type of the ENT :
     eg : B-ORG -> B-org"""
    if anno.endswith("PERSON"):
        return anno[:2] + "pers"

    elif anno.endswith("ORG"):
        return anno[:2] + "org"

    elif anno.endswith("WORK_OF_ART"):
        return anno[:2] + "prod"

    elif anno.endswith("DATE"):
        return anno[:2] + "time"

    elif anno.endswith("FAC") or anno.endswith("GPE") or anno.endswith("LOC"):
        return anno[:2] + "loc"
    else:
        # EVENT
        # WORK OF ART
        # LAW
        # LANGUAGE
        # TIME
        # PERCENT
        # MONEY
        # QUANTITY
        # ORDINAL
        # CARDINAL
        return "O"


def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i : i + n]


def _output(text, out_file=None):
    if out_file:
        print(text, file=open(out_file, "w"))
        print("outfile:\t{}".format(out_file))
    else:
        return text


class ConllReader:
    def __init__(self, conll_file):
        self.conll = open(conll_file).read()
        self.conll_file_name = [conll_file]
        self.attrs = self.conll.splitlines()[0].split(
            "\t"
        )  # get the TOKEN, NE-COARSE-LIT , ....
        self.json = self._to_json()

    def __str__(self):
        return (
            str(self.conll_file_name)
            + "\n\t"
            + str(len(self.conll))
            + " characters\n\t"
            + str(
                len(
                    [tok for doc in self for tok in doc["lis_tokens"] if tok["IS_WORD"]]
                )
            )
            + " tokens\n\t"
            + str(len(self.conll.splitlines()))
            + " lines\n\t"
            + str(len(self.json))
            + " documents\n\t"
            + str(len(self.conll_file_name))
            + " files"
        )

    __repr__ = __str__

    def __len__(self):
        return len(self.json)

    def __iter__(self):
        for doc in self.json:
            yield doc

    def list_doc_id(self):
        """return the list of the documents composing the object"""
        return [doc["document_id"] for doc in self]

    def add_file(self, fname):
        """add a fine to the db (eg to add the sample text"""
        new_conll = ConllReader(fname)
        self.conll += new_conll.conll
        self.conll_file_name += new_conll.conll_file_name
        self.json += new_conll.json

    def translate(self, source, dest):
        """translate the date to another language"""
        translator = Translator()
        for doc in tqdm([doc for doc in self if doc["language"] == source]):
            str_ = " ".join([tok["TOKEN"] for tok in doc["lis_tokens"]])
            try:
                trans_res = translator.translate(str_, src=source, dest=dest).text
            except Exception as e:
                print(e)
                trans_res = str_
            for tok, trans_tok in zip(doc["lis_tokens"], trans_res.split()):
                tok["TOKEN"] = trans_tok

    def correct_spelling(self):
        """correct the spelling of the tokens"""
        for doc in tqdm(self):
            for tok in tqdm(doc["lis_tokens"]):
                tok["SPELL_CHECK"] = spell.correction(tok["TOKEN"])

    def dump_spelling(self, out_file=None):
        """dump the spelling into a json file"""
        dic = {
            doc["document_id"]: [tok["SPELL_CHECK"] for tok in doc["lis_tokens"]]
            for doc in self
        }
        _output(json.dumps(dic, indent=4), out_file)

    def load_spelling(self, in_file="../results/spelling.json"):
        """load the corrected file"""
        dic_spell = json.load(open(in_file))
        for doc in self:
            try:
                assert len(doc["lis_tokens"]) == len(dic_spell[doc["document_id"]])
            except (AssertionError, KeyError):
                print("error with " + doc["document_id"] + "\t" + doc["language"])
            else:
                for tok, correct in zip(
                    doc["lis_tokens"], dic_spell[doc["document_id"]]
                ):
                    tok["SPELL_CHECK"] = correct

    def _shuffle(self):
        random.shuffle(self.json)

    def drop_nonwords(self):
        """detele the non words in the json"""
        for doc in self:
            doc["lis_tokens"] = [
                tok
                for tok in doc["lis_tokens"]
                if tok["IS_WORD"] and not tok["TOKEN"].startswith("#")
            ]

    def to_text(self, out_file=None, keep_segment_iiif=True):
        """returns the text of the file (easier to read for human)"""
        str_ = ""
        for doc in self:
            str_ += self.document_to_text(doc) + "\n" * 2
        return _output(str_, out_file)

    @staticmethod
    def document_to_text(doc: dict, out_file=None, keep_segment_iiif=True):
        """takes a document (a element of the json list) and return the text"""
        str_ = ""
        for key in ("language", "newspaper", "date", "document_id"):
            str_ += "# " + key + " = " + doc[key] + "\n"
        for i, token in enumerate(doc["lis_tokens"]):
            if not i:
                continue
            if token["TOKEN"].startswith("# segment_iiif_link") and keep_segment_iiif:
                str_ += "\n"
                continue
            str_ += token["TOKEN"]
            if token["MISC"] == "endOfLine":
                str_ += "\n"
            elif token["MISC"] == "NoSpaceAfter":
                continue
            else:
                str_ += " "
        return _output(str_, out_file)

    def print_random_article(self, out_file=None):
        """display a random article"""
        str_ = self.document_to_text(random.choice(self.json))
        print(_output(str_, out_file))

    def _to_json(self, out_file=None):
        """return a json file of the file"""
        json_txts = []
        for i, line in enumerate(self.conll.splitlines()):
            # blank line
            if not line:
                continue
            # new text
            if line.startswith("# language ="):
                token_position = 0
                try:
                    json_txts.append(jdata)
                    jdata = {"lis_tokens": []}
                except UnboundLocalError:
                    jdata = {"lis_tokens": []}
            # first line skipped
            if not i:
                continue
            # comments in the conll file eg newspaper, date, document_id ...
            elif line.startswith("#") and not line.startswith("# segment_iiif_link"):
                try:
                    attr, val = re.findall(r"#\s(\w+)\s=\s(\w+.*)", line)[0]
                except IndexError:
                    # the line starts with a # but is not a comment (eg : # is a token of the text)
                    new_tok = {}
                    new_tok["IS_WORD"] = True
                    for at, char in zip(self.attrs, line.split("\t")):
                        new_tok[at] = char
                    new_tok["POSITION"] = token_position
                    token_position += 1
                    jdata["lis_tokens"].append(new_tok)
                else:
                    jdata[attr] = val
            # blank line
            elif line.startswith("# segment_iiif_link"):
                new_tok = {}
                new_tok["IS_WORD"] = False
                for at in self.attrs:
                    new_tok[at] = "NA"
                new_tok["TOKEN"] = line  # XXX
                new_tok["POSITION"] = token_position
                # token_position+=1
                jdata["lis_tokens"].append(new_tok)
            # token
            else:
                new_tok = {}
                new_tok["IS_WORD"] = True
                for at, char in zip(self.attrs, line.split("\t")):
                    new_tok[at] = char
                new_tok["POSITION"] = token_position
                token_position += 1
                jdata["lis_tokens"].append(new_tok)
        # add the last text
        json_txts.append(jdata)
        assert len(json_txts) == len(
            re.findall("# language =", self.conll)
        ), "a document is missing"

        return json_txts

    def to_conll(
        self, out_file=None,
    ):
        """export the object (lis of tokens , doc id...) into a tsv file"""
        tsv_file = "\t".join(self.attrs) + "\n"
        for i, doc in enumerate(self.json):
            tsv_file += """# language = """ + doc["language"] + """\n"""
            tsv_file += """# newspaper = """ + doc["newspaper"] + """\n"""
            tsv_file += """# date = """ + doc["date"] + """\n"""
            tsv_file += """# document_id = """ + doc["document_id"] + """\n"""
            for tok in doc["lis_tokens"]:
                if tok["IS_WORD"]:
                    try:
                        tsv_file += "\t".join([tok[att] for att in self.attrs])
                    except:
                        print(self.attrs)
                        print(tok)
                        input()
                else:
                    # print(tok)
                    tsv_file += tok["TOKEN"]
                tsv_file += "\n"
            if i == len(self.json) - 1:
                return _output(tsv_file, out_file)
            tsv_file += "\n"
        tsv_file += "\n"

    # def to_basic_conll(self, fields=["NE-COARSE-LIT"], out_file=None):
    #     """return a basic conll file (to train a BERT model):
    #         token\tcat each document is separated with a blank line """
    #     str_ = ""
    #     for doc in self:
    #         for i, token in enumerate(doc["lis_tokens"]):
    #             # dirty way to bypass BERT 512 tokenization
    #             if not i % 300:
    #                 str_ += "\n"
    #             if token["IS_WORD"]:
    #                 str_ += token["TOKEN"]
    #                 for field in fields:
    #                     str_ += "\t" + token[field]
    #                 str_ += "\n"
    #             if token["MISC"] == "EndOfLine":
    #                 pass
    #         str_ += "\n"
    #     return _output(str_, out_file)


from qwikidata.sparql import return_sparql_query_results


def get_item_ID(name):
    name = " ".join([i.capitalize() for i in name.split()])
    sparql_query = (
        """SELECT ?item WHERE {
                ?sitelink schema:about ?item;
                schema:isPartOf <https://fr.wikipedia.org/>;
                schema:name '"""
        + name
        + """'@fr.}
            """
    )
    try:
        res = return_sparql_query_results(sparql_query)
        item = res["results"]["bindings"][0]["item"]["value"]
        mid = item.split("/")[-1]
    except:
        mid = None
    return mid


import time


def make_crosswiki(path):
    import numpy as np
    from multiprocessing import Pool

    cm = np.load(path + "cm.fr.npy", allow_pickle=True, encoding="latin1").item()
    mentions = {}
    history = {}
    tic = time.time()
    keys = list(cm.keys())
    print("found:{}".format(len(keys)))
    """print(keys[:100])
    entities=[i[1]for i in keys]
    with Pool(1) as p:
        wds=p.map(get_item_ID, entities[:1000])
    print(len(wds),len([i for i in wds if i!=None]))
    exit()"""
    toc = time.time()
    print("getting ids took:{}s".format(toc - tic))
    gm = 0
    for i in tqdm.tqdm(range(len(cm))):
        me = keys[i]
        m, e = me
        try:
            wd = history[e]
        except:
            wd = get_item_ID(e)  # getWikipediaIdWikidataId(e)
            history[e] = wd
        # wd=wds[i]
        if wd == None:
            continue
        gm += 1
        try:
            mentions[m] += [(wd, cm[me])]
        except:
            mentions[m] = [(wd, cm[me])]
        # if i>1000:break
    print("found:{}".format(gm))
    np.save(path + "crosswiki.fr.npy", mentions)


def wikidata_bigraph(filename="/nfs/nas4/cicoda/cicoda/wikidata_translation_v1.tsv.gz"):
    import gzip
    import bz2
    import time
    from tqdm import tqdm
    import numpy as np
    import pandas as pd

    tic = time.time()
    df = pd.read_csv(
        "/nfs/nas4/cicoda_tmp/cicoda_tmp/HIPE/hipe2020/data/bigFiles/wpedia2widata.tsv",
        sep="\t",
    )
    n = len(df)
    cands = {
        df["wdata"][i]: df["wpedia"][i] for i in range(n)
    }  # df.set_index('wdata').to_dict()
    print("Using cands of size:{}".format(len(df)))
    print(cands.keys()[:10])
    if ".gz" in filename:
        fopen = lambda filename: gzip.open(filename, "rb")
    elif ".bz2" in filename:
        fopen = lambda filename: bz2.BZ2File(filename, "r")
    else:
        fopen = lambda filename: open(filename, "r")
    n = 143121769 // 2
    pbar = tqdm(total=n)
    dic = {}
    for i in fopen(filename):
        pbar.update(1)
        try:
            line = i.split("\n")[0].split("\t")
            word = line[0]
            vec = np.array(map(float, line[1:]))
            if "/entity" in word:
                word = word.split("/")[-1].replace(">", "")
                try:
                    _ = cands[word]
                except:
                    continue
                # print(word);exit()
                dic[word] = vec
            # if "@fr" in word:
            #     word=word.replace("@fr","")
            #     #print(word);exit()
            #     dic[word]=vec
        except:
            pass
        # if len(dic)>1000:break
    toc = time.time()
    print("Took:{}".format(toc - tic))
    np.save(
        "/nfs/nas4/cicoda_tmp/cicoda_tmp/HIPE/hipe2020/data/bigFiles/wikidata_bigraph.entity.npy",
        dic,
    )


def get_Q_from_URL(url):
    """return the associated Q number from a wikipedia URL
    get_Q_from_URL('https://en.wikipedia.org/wiki/Barack_Obama') -> 'https://www.wikidata.org/wiki/Q76'
    """
    r = requests.get(url)
    match = re.search(
        r"(?<=\Whttps://www\.wikidata.org/wiki/).*?(?P<q_num>Q\d\d+)(?=\W)", r.text
    )
    if match:
        return match.group("q_num")


if __name__ == "__main__":

    cr = ConllReader(
        "/home/k/Documents/hipe2020/data/training-v1.0/fr/HIPE-data-v1.0-train-fr.tsv"
    )
    cr.print_random_article()

    # # print(cr.list_doc_id())

    cr.add_file("../data/release-v01/fr/HIPE-data-v01-sample-fr.tsv")
    cr.add_file("../data/training-v1.0/fr/HIPE-data-v1.0-dev-fr.tsv")

    cr.correct_spelling()
    cr.dump_spelling("../results/spelling.json")
