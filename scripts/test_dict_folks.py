#### #### #### that script is used to test, feel free to modify / change everything
from pprint import pprint
from pipeline import *
from params import get_parser

# from NER_crf import Hipe_CRF
import json
from glob import glob
from touch import touch
from datetime import datetime
from pprint import pprint

res_file = f"../results/res_copains_{datetime.now()}.json"
try:
    touch(res_file)
except Exception as e:
    print(f"couldn't touch {res_file}")
    print(e)
    raise

parser = get_parser()

args_dict, unknown = parser.parse_known_args()
args_dict.verbose = True
# args_dict.fastText_vectors=True

# args_dict.train='../training-v1.0/fr/HIPE-data-v1.0-train-fr.tsv'
# args_dict.dev='../training-v1.0/fr/HIPE-data-v1.0-dev-fr.tsv'

dict_res = {}

pprint(args_dict)

pipe = Pipeline(args_dict)

pipe.train_predict()
pipe.link_conll(pipe.dev_file_conll)

# for dic in sorted(glob('dict_folks*'), reverse=True):
for dic in sorted(["dict_folks_th_0.1-0.8.pkl"], reverse=True):
    print(dic)
    pipe.load_dict_folks(dic)
    pipe.rule_look_for_ents_with_dict()
    pipe.print_results()
    pipe.get_results()

    dict_res[dic] = {
        """["NE-COARSE-LIT"]["ALL"]["strict"]["F1_micro"]""": pipe.results[
            "NE-COARSE-LIT"
        ]["ALL"]["strict"]["F1_micro"],
        """["NE-COARSE-LIT"]["ALL"]["ent_type"]["F1_micro"]""": pipe.results[
            "NE-COARSE-LIT"
        ]["ALL"]["ent_type"]["F1_micro"],
    }
    json.dump(dict_res, open(res_file, "w"), indent=4, sort_keys=True)
    pprint(dict_res)
    pipe.train_predict()
