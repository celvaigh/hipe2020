import requests, json, tqdm
from fake_useragent import UserAgent
import numpy as np
from concurrent.futures import ThreadPoolExecutor
from multiprocessing import Pool
import pandas as pd


def redirect(name):
    S = requests.Session()
    URL = "https://en.wikipedia.org/w/api.php"
    PARAMS = {"action": "query", "format": "json", "titles": name, "prop": "redirects"}
    R = S.get(url=URL, params=PARAMS)
    DATA = R.json()
    PAGES = DATA["query"]["pages"]
    for k, v in PAGES.items():
        for re in v["redirects"]:
            print(re["title"] + " redirect to " + v["title"])


def getWikidataId(name):
    url = "https://fr.wikipedia.org/w/api.php?action=query&prop=pageprops&titles={}&format=json".format(
        "_".join(str(name).capitalize().split(" "))
    )
    ua = UserAgent()
    headers = {
        "User-Agent": ua.random,
    }
    try:
        res = json.loads(requests.get(url, headers=headers).text)
        wpid = list(res["query"]["pages"].keys())[0]
        wdid = res["query"]["pages"][wpid]["pageprops"]["wikibase_item"]
        return wdid
    except:
        return "NIL"


def create_cands():
    dic = np.load(
        "/nfs/nas4/cicoda_tmp/cicoda_tmp/HIPE/hipe2020/data/bigFiles/cm.fr.npy",
        allow_pickle=True,
    ).item()
    mentions = {}

    # entities=[]
    # for i in tqdm.tqdm(dic):
    #     entities+=[i[1]]
    # entities=list(set(entities))
    # nonil=0
    # #e2n={entities[i]:i for i in tqdm.tqdm(range(len(entities)))}
    # print("entities:{}".format(len(entities)))
    # db=pd.read_csv('/nfs/nas4/cicoda_tmp/cicoda_tmp/entitylinking/mid2wikipedia.tsv',sep="\t")
    # name=db["en_name"]
    # curid=db["curid"]
    # mids={name[i]:curid[i]for i in tqdm.tqdm(range(len(db)))}
    # print(len(mids))
    # executor = ThreadPoolExecutor(max_workers=30)
    # res=executor.map(getWikidataId, tqdm.tqdm(entities))
    # executor = Pool(32)
    # for e in  tqdm.tqdm(entities):
    #     mids[e]=get_candidate(e)
    #     if mids[e]!="NIL":nonil+=1
    # res=executor.map(getWikidataId,)
    # pbar=tqdm.tqdm(total=len(entities))
    # for r in res:
    #     name,wid=r
    #     mids[name]=wid
    #     if wid!="NIL":nonil+=1
    #     pbar.update(1)

    # for i in tqdm.tqdm(dic):
    #     try:_=mids[i[1]];nonil+=1
    #     except:continue
    #     try:mentions[i[0]]+=[(mids[i[1]],dic[i])]
    #     except:mentions[i[0]]=[(mids[i[1]],dic[i])]
    for i in tqdm.tqdm(dic):
        try:
            mentions[i[0]] += [(i[1], dic[i])]
        except:
            mentions[i[0]] = [(i[1], dic[i])]
    for i in tqdm.tqdm(mentions):
        mentions[i] = sorted(mentions[i], key=lambda x: x[1], reverse=True)
    # print("entities not nil:{}".format(nonil))
    output = (
        "/nfs/nas4/cicoda_tmp/cicoda_tmp/HIPE/hipe2020/data/bigFiles/cands_cm.fr.npy"
    )
    np.save(output, mentions)
    print(output)


if __name__ == "__main__":
    create_cands()
