#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import requests, json, tqdm
from fake_useragent import UserAgent
import numpy as np
from concurrent.futures import ThreadPoolExecutor
import re, sys
import string
import heapq
import warnings
from hipe_utils import ConllReader, make_crosswiki, get_Q_from_URL
from bs4 import BeautifulSoup
from gensim.models import KeyedVectors
from scipy import spatial
from qwikidata.sparql import return_sparql_query_results
from difflib import SequenceMatcher
import jellyfish
from glob import glob
import wikipedia

# from imblearn.under_sampling import RepeatedEditedNearestNeighbours
# from imblearn.under_sampling import ClusterCentroids
from sklearn.utils import class_weight
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import (
    GradientBoostingClassifier,
    RandomForestClassifier,
    AdaBoostClassifier,
    VotingClassifier,
)
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
import time
import os
from gensim.models.fasttext import FastText
from multiprocessing import Pool
import multiprocessing

from fuzion import fuzion
import unidecode
import logging

logging.basicConfig(
    level=logging.DEBUG,
    format="[%(asctime)s] - [%(levelname)s] - %(message)s",
    handlers=[logging.FileHandler(__file__ + ".log"), logging.StreamHandler()],
)


logger = logging.getLogger(__name__)


def getWikipediaIdWikidataId(lang, name):
    url = "https://{}.wikipedia.org/w/api.php?action=query&prop=pageprops&titles={}&format=json".format(
        lang, "_".join(str(name).split(" "))
    )
    ua = UserAgent()
    headers = {
        "User-Agent": ua.random,
    }
    try:
        res = json.loads(requests.get(url, headers=headers).text)
        wpid = list(res["query"]["pages"].keys())[0]
        wdid = res["query"]["pages"][wpid]["pageprops"]["wikibase_item"]
        return wdid
    except:
        return "NIL"


def seq_similar(a, b):
    return SequenceMatcher(None, a, b).ratio()


def get_entity(entity):
    # From https://www.wikidata.org/wiki/Wikidata:SPARQL_query_service/queries/examples#Cats
    sparql_query = (
        """SELECT ?typeLabel WHERE {wd:"""
        + entity
        + """ wdt:P31/wdt:P279* ?type. SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }}"""
    )
    res = return_sparql_query_results(sparql_query)
    # print(i["typeLabel"]["value"])
    return [i["typeLabel"]["value"] for i in res["results"]["bindings"]]


def get_item_ID(name):
    name = " ".join([i.capitalize() for i in name.split()])
    sparql_query = (
        """SELECT ?item WHERE {
                ?sitelink schema:about ?item;
                schema:isPartOf <https://fr.wikipedia.org/>;
                schema:name '"""
        + name
        + """'@fr.}
            """
    )
    try:
        res = return_sparql_query_results(sparql_query)
        item = res["results"]["bindings"][0]["item"]["value"]
        mid = item.split("/")[-1]
    except:
        mid = None
    return mid


class Classifier:
    def __init__(self, X_train, y_train, args_dict, X_v=None, y_v=None):
        self.X_train, self.y_train = X_train, y_train
        self.X_v, self.y_v = X_v, y_v
        self.args_dict = args_dict
        # print("Under sampling data ...")
        # y=np.array([i[0] for i in self.y_train])
        # class_weights = class_weight.compute_class_weight('balanced', np.unique(y),y)
        # print(class_weights)
        # renn = RepeatedEditedNearestNeighbours()
        # self.X_train,self.y_train = renn.fit_resample(self.X_train,self.y_train)
        # cc = ClusterCentroids(random_state=0)
        # self.X_train,self.y_train = cc.fit_resample(self.X_train,self.y_train)
        params = dict(
            {
                "n_estimators": 1000,
                "max_leaf_nodes": 4,
                "max_depth": None,
                "random_state": 2,
                "min_samples_split": 5,
            }
        )
        if args_dict.classifier == "REG":
            print("Training the classifier using LogisticRegression")
            self.model = LogisticRegression(
                random_state=0, solver="lbfgs", multi_class="multinomial"
            )
        elif args_dict.classifier == "GB":
            print("Training the classifier using GradientBoostingClassifier")
            self.model = GradientBoostingClassifier(**params)
        elif args_dict.classifier == "RF":
            print("Training the classifier using RandomForestClassifier")
            self.model = RandomForestClassifier(
                criterion="entropy", max_depth=15, min_samples_leaf=5, n_jobs=-1
            )
        elif args_dict.classifier == "MLP":
            print("Training the classifier using MLPClassifier")
            self.model = MLPClassifier(
                solver="lbfgs", alpha=1e-5, hidden_layer_sizes=(5, 2), random_state=1
            )  # ,workers=args_dict.workers)
        elif args_dict.classifier == "QDA":
            print("Training the classifier using QuadraticDiscriminantAnalysis")
            self.model = QuadraticDiscriminantAnalysis()
        elif args_dict.classifier == "KNN":
            print("Training the classifier using KNeighborsClassifier")
            self.model = KNeighborsClassifier(3)
        elif args_dict.classifier == "SVM":
            print("Training the classifier using SVM")
            self.model = SVC(gamma=2, C=1, probability=True)
        elif args_dict.classifier == "ADAB":
            print("Training the classifier using AdaBoostClassifier")
            self.model = AdaBoostClassifier(
                DecisionTreeClassifier(max_depth=1), algorithm="SAMME", n_estimators=200
            )
        elif args_dict.classifier == "VC":
            lr = LogisticRegression(
                random_state=0, solver="lbfgs", multi_class="multinomial"
            )
            rf = RandomForestClassifier(
                criterion="entropy", max_depth=15, min_samples_leaf=5, n_jobs=-1
            )
            mlp = MLPClassifier(
                solver="lbfgs", alpha=1e-5, hidden_layer_sizes=(5, 2), random_state=1
            )
            qd = QuadraticDiscriminantAnalysis()
            ac = AdaBoostClassifier(
                DecisionTreeClassifier(max_depth=1), algorithm="SAMME", n_estimators=200
            )
            clfs = set([lr, rf, mlp, qd, ac])
            self.model = VotingClassifier(estimators=clfs, voting="soft")
        else:
            print("Classifier {} not found".format(args_dict.classifier))
        if args_dict.classifier != "KERAS":
            self.model.fit(self.X_train, self.y_train)

    def predict(self, x):
        if self.args_dict.classifier == "KERAS":
            label = self.model.predict_classes(x)[0]
            p = self.model.predict(x)
        else:
            label = self.model.predict(x)
            p = self.model.predict_proba(x)
        return label, p


def get_start(args_dic, testname, test=False):
    start = 0
    if test:
        regex = args_dic.path + "/data/bigFiles/{}.cel*".format(testname)
    else:
        regex = args_dic.path + "/data/bigFiles/*train.cel*"
    for file in glob(regex):
        try:
            x = int(file.split(".")[2].split("-")[1])
        except:
            continue
        start = max(start, x)
    return start + 1


def clean(s):
    us = unidecode.unidecode(s).lower()
    return "".join(filter(str.isalnum, us))


class Linker:
    def __init__(self, args_dict):
        self.name = "Linker0"
        self.path = args_dict.path
        self.lang = args_dict.lang
        self.train_file = self.path + args_dict.train
        # self.dev2=self.path+args_dict.dev
        self.dev = self.path + args_dict.dev
        self.output = self.path + args_dict.output
        self.embed_path = args_dict.embed_file
        self.data = []
        self.docs = []
        self.features = {}
        self.history = {}
        self.args_dict = args_dict
        self.nil_th = 0.5
        self.topk = args_dict.topk

        self.htype2wtype = {
            "loc": "geographic location",
            "org": "organization",
            "pers": "human",
            "prod": "product",
        }
        self.wtype2htype = {v: k for k, v in self.htype2wtype.items()}
        self.load()
        if "masked" in self.dev:
            self.test_alis = "test-masked"
        else:
            self.test_alis = "test-noner"
        # tic=time.time()
        # print(self.generate_candidates_from_wikipedia("france"))
        # toc=time.time()-tic
        # logger.info("Generation time:{:0.3f}s".format(toc))
        # self.generate(False)

        # self.check_generation(self.dev)
        # self.check_generation(self.train_file)
        # self.save()

    def generate(self, test):

        if test:
            filename = self.dev
        else:
            filename = self.train_file
        _start = get_start(self.args_dict, self.test_alis, test=test)
        logger.info("Starting from:{}".format(_start))
        self.get_cel_data(filename, start=_start, test=test)

    def get_hipe_label(self, kb_id):
        """takes an id from the KB and returns the first label that is similar to a hipe label,
        returns 'None' (as a string) if no label is found
        eg:
           get_hipe_label('Q76') -> pers
           get_hipe_label('Q76111111111111111111') -> 'None'
        """
        for wtype in self.types.get(kb_id, []):
            if wtype in self.wtype2htype:
                return self.wtype2htype[wtype]
        else:
            return "None"

    def generate_candidate(self, name):

        try:
            return self.cands[name]
        except:
            pass
        # ua = UserAgent()
        # headers = {'User-Agent': ua.random,}
        url = "https://www.wikidata.org/w/index.php?search=&search={}&title=Special%3ASearch&go=Go&ns0=1&ns120=1".format(
            name
        )
        r = requests.get(url)  # , headers=headers)
        # print(r.text)
        soup = BeautifulSoup(r.text, "html.parser")
        # itemlinks=soup.find_all('span', class_="wb-itemlink-id")
        links = soup.find_all("a")
        cands = []
        for link in links:
            a = link.get("href")
            if a != None and a.startswith("/wiki/Q"):
                title = link.get("title")
                if "|" in title:
                    x = title.split(" | ")
                    t = x[0]
                else:
                    t = title
                t = "".join([x for x in t if x in string.printable])
                t, name = t.lower(), name.lower()
                # print(name,t)
                try:
                    pop = self.cm[(name, t)]
                except:
                    pop = 0
                seq_sim = seq_similar(name, t)
                lev_d = jellyfish.levenshtein_distance(name, t)
                jaro_d = jellyfish.jaro_distance(name, t)
                dam_d = jellyfish.damerau_levenshtein_distance(name, t)
                name = "_".join(name.split(" "))
                subs = int(name in t) + int(t in name)
                t = "_".join(t.split(" "))
                try:
                    sim = 1 - spatial.distance.cosine(
                        self.wtranse[name], self.wtranse[t]
                    )
                except Exception as e:
                    sim = 0
                cands += [
                    [
                        name,
                        t,
                        a.replace("/wiki/", ""),
                        sim,
                        pop,
                        seq_sim,
                        lev_d,
                        jaro_d,
                        dam_d,
                        subs,
                    ]
                ]
        # cands.sort(key=lambda x:x[4])
        self.cands[name] = cands
        return cands

    def best_generation(self, name):
        try:
            return self.cands[name]
        except:
            pass
        cands = []
        try:
            ccands = self.crossfr[name.lower()]
            WSEARCH = False
        except:
            ccands = wikipedia.search(name)
            WSEARCH = True
        for c in ccands:
            if WSEARCH:
                wid = self.get_wikidata(c[0])
                t = c
            else:
                wid = self.get_wikidata(c)
                t = c[0]
            if wid != "NIL":
                if WSEARCH:
                    pop = self.cm[(name, c)]
                else:
                    pop = c[1]
                seq_sim = seq_similar(name, t)
                lev_d = jellyfish.levenshtein_distance(name, t)
                jaro_d = jellyfish.jaro_distance(name, t)
                dam_d = jellyfish.damerau_levenshtein_distance(name, t)
                name = "_".join(name.split(" "))
                subs = int(name in t) + int(t in name)
                t = "_".join(t.split(" "))
                try:
                    sim = 1 - spatial.distance.cosine(
                        self.wtranse[name], self.wtranse[t]
                    )
                except Exception as e:
                    sim = 0
                cands += [[name, t, wid, sim, pop, seq_sim, lev_d, jaro_d, dam_d, subs]]
        self.cands[name] = cands
        return cands

    def dic_only(self, name):
        try:
            self.history[name] = self.exactLinker[name]
        except:
            self.history[name] = "NIL"

    def wiki_only(self, name):
        self.history[name] = getWikipediaIdWikidataId("fr", name.capitalize())

    def dic_wiki(self, name):
        self.dic_only(name)
        if self.history[name] == "NIL":
            self.wiki_only(name)

    def wiki_dic(self, name):
        self.wiki_only(name)
        if self.history[name] == "NIL":
            self.dic_only(name)

    def get_baseline(self, name):
        if self.args_dict.baseline == "dic_only":
            return self.dic_only(name)
        elif self.args_dict.baseline == "wiki_only":
            return self.wiki_only(name)
        elif self.args_dict.baseline == "wiki_dic":
            return self.wiki_dic(name)
        elif self.args_dict.baseline == "dic_wiki":
            return self.dic_wiki(name)
        else:
            print("Unknown baseline:{}".format(self.args_dict.baseline))

    def has_type(self, mid, typ):
        try:
            types = self.types[mid]
        except:
            try:
                types = get_entity(mid)
                self.types[mid] = set(types)
            except:
                return 0
        try:
            htyp = self.htype2wtype[typ]
        except:
            return 0
        return int(htyp in self.types[mid])

    def get_test(self, cands, typ, min_c=-1):
        features_c = []
        if min_c > 0:
            nc = min(min_c, len(cands))
        else:
            nc = len(cands)
        for eid in range(nc):
            features = cands[eid]
            cmid = features[2]
            sametype = self.has_type(cmid, typ)
            if not sametype:
                continue
            features = list(features[3:-4]) + [str(sametype)]
            x = np.array(features)
            if self.args_dict.features != "":
                features = list(map(int, self.args_dict.features.split(",")))
                x = np.array([x[i] for i in features])
            features_c += [x]
        return np.array(features_c).astype(np.float)

    def _link(self, cands, typ):
        # get the features
        x_test = self.get_test(cands, typ)  # ,min_c=10)
        if not len(x_test):
            return "NIL"
        label, p = self.classifier.predict(x_test)
        p0, p1 = p[:, 0], p[:, 1]
        if sum(label):  # one of them is 1 at least
            best_c = np.argmax(p1)  # best candidate from the classifier
            mid = cands[best_c][2]
        else:
            best_c = np.argmin(p0)  # farest from non candidates
            if best_c < self.nil_th:
                mid = cands[best_c][2]
            else:
                mid = "NIL"
        return mid

    def _linktopk(self, cands, typ):

        if self.topk == 1:
            return self._link(cands, typ)
        # get the features
        x_test = self.get_test(cands, typ)  # ,min_c=10)
        # get the qids
        qids = [x[2] for x in cands]

        if not len(x_test):
            return "NIL"
        label, p = self.classifier.predict(x_test)
        p0, p1 = p[:, 0], p[:, 1]
        best0 = [
            x for _, x in sorted(zip(p0, qids), key=lambda pair: pair[0], reverse=True)
        ]
        best1 = [x for _, x in sorted(zip(p0, qids), key=lambda pair: pair[0])]
        if sum(label):
            qids = best1[:6]
        else:
            qids = best0[:6]
        if len(qids) < 5:
            qids += ["NIL"] * (5 - len(qids))
        return "|".join(qids)

    def link_doc(self, tmpdoc, doci):
        tokens = tmpdoc["lis_tokens"]
        mentions = 0
        for tm in self.dic_test:
            if not "mention_" in tm:
                continue
            x = tm.split("_")[1]
            if "mention_{}_doc_{}".format(x, doci) == tm:
                mention = self.dic_test[tm]
            else:
                continue
            name = mention[0][0].lower()
            ids = list(map(int, mention[1][0].split(";")))
            try:
                x_test = self.dic_test[tm.replace("mention_", "x_")]
                # x_test=np.array([x[3:]for x in x_test]).astype(np.float)
            except:
                continue
            try:
                typ = tmpdoc["lis_tokens"][ids[0]]["NE-COARSE-LIT"][2:]
            except:
                print(mention, tm, ids, len(tokens))
                exit()
            if typ == "time":
                self.history[name] = "NIL"
            if not len(typ):
                continue  # logger.info("Found type:{}".format(name))
            # self.history[name]='NIL'
            try:
                mid = self.history[name]
            except:
                self.get_baseline(name)
                if self.history[name] == "NIL":
                    self.history[name] = self._linktopk(x_test, typ)
                # if self.history[name]=="NIL":self.cross_wiki(name)
                mid = self.history[name]
            if self.topk > 1:
                if not "|" in self.history[name]:
                    self.history[name] = "|".join(5 * [self.history[name]])
            mid = self.history[name]
            for idx in ids:
                try:
                    tmpdoc["lis_tokens"][idx]["NEL-LIT"] = "NIL"  # erase any value
                    tmpdoc["lis_tokens"][idx][
                        "NEL-LIT"
                    ] = mid  # put the predicted entity
                    tmpdoc["lis_tokens"][idx][
                        "NEL-METO"
                    ] = mid  # put the predicted entity
                except Exception as e:
                    print("index problem", ids, name, e)
                    exit()
        return tmpdoc, mentions

    def get_sample(self, min_c=-1, test=False):
        X, y, d = fuzion(self.args_dict, test=test)
        # print(X[0])
        positive = sum(y)
        samples = len(y)
        target = "test" if test else "train"
        print(
            "{} size:{}, positive examples:{} and negative examples:{}".format(
                target, samples, positive, samples - positive
            )
        )
        features_c = []
        X = np.array([x[3:] for x in X])
        if self.args_dict.features != "":
            features = list(map(int, self.args_dict.features.split(",")))
            X = np.array([np.array([x[i] for i in features]) for x in X])
        return X.astype(np.float), y.astype(np.float), d

    def link_conll(self, cr):
        ccr = cr.json
        doci = 0
        tic = time.time()
        X_train, y_train, _ = self.get_sample(test=False)
        _, _, self.dic_test = fuzion(self.args_dict, test=True)
        self.classifier = Classifier(X_train, y_train, self.args_dict)
        toc = time.time() - tic
        logger.info("Training time:{:0.3f}s".format(toc))
        tic = time.time()
        corpus_mentions = 0
        for doc in tqdm.tqdm(ccr):
            cr.json[doci], mentions = self.link_doc(doc, doci)
            corpus_mentions += mentions
            doci += 1
        toc = time.time() - tic
        logger.info("Prediction time:{:0.3f}s".format(toc))
        logger.info(
            "Avg mention per doc:{:0.3f} mentions".format(corpus_mentions / doci)
        )
        self.dev_file_conll = cr
        try:
            cr.to_conll(self.output)
        except Exception as e:
            warnings.warn(f"""couldn't create {self.output} : {e}""")
        # print(self.classifier.model.coef_)

    def get_wikidata(self, name):
        try:
            return self.name2wikidata[name]
        except:
            wid = getWikipediaIdWikidataId("fr", name.capitalize())
            if wid == "NIL":
                wid = getWikipediaIdWikidataId("en", name.capitalize())
            self.name2wikidata[name] = wid
            return wid

    def check_generation(self, ccr):
        # ccr=ConllReader(filename).json
        avgm, nonil = 0, 0
        i = 0
        for doc in tqdm.tqdm(ccr):
            tokens = doc["lis_tokens"]
            mentions = self.get_mentions(tokens, test=False)
            keys = list(mentions.keys())
            for mention in keys:
                if mention[1] == "NIL":
                    continue
                try:
                    cands, _ = self.crosscands[clean(mention[0])]  #
                except:
                    cands = []
                try:
                    ccands = self.crossfr[mention[0].lower()]
                except:
                    ccands = []
                for c in ccands:
                    wid = self.get_wikidata(c[0])
                    if wid != "NIL":
                        cands += [wid]
                if len(cands) == 0:
                    cands = [i[2] for i in self.generate_candidate(mention[0])]
                inc = int(mention[1] in cands)
                if not inc:
                    cands = wikipedia.search(mention)
                    for wcand in cands:
                        # try:wid=wikipedia.page(wcand).pageid
                        # except:continue#wid="NIL"
                        if inc:
                            break
                        try:
                            if mention[1] == getWikipediaIdWikidataId("fr", wcand):
                                inc = 1  # self.wikipedi2wikidata[wid]:inc=1;break
                        except:
                            pass
                avgm += inc
                nonil += 1
                if inc == 0:
                    logging.info("Not found:{}".format(mention))
            logging.info("Prec generation is {:0.3f}".format(avgm / nonil))

    def get_mentions(self, tokens, test=False):
        tokid, mentions = 0, {}
        while tokid < len(tokens):
            token = tokens[tokid]
            name, typ, ids = "", "", []
            while token["NE-COARSE-LIT"] != "O":
                # We have a mention not a comment
                if token["NE-COARSE-LIT"].startswith("B-") or token[
                    "NE-COARSE-LIT"
                ].startswith("I-"):
                    if len(name) == 0:
                        name += token["TOKEN"] + " " * (
                            1 - int(token["MISC"] == "NoSpaceAfter")
                        )
                    else:
                        name += token["TOKEN"] + " " * (
                            1 - int(token["MISC"] == "NoSpaceAfter")
                        )
                    if not len(typ):
                        typ = token["NE-COARSE-LIT"][2:].lower()
                        mid = token["NEL-LIT"]
                        # if mid=="_":print(token)
                        # if test:mid="NIL"
                    ids += [str(tokid)]
                tokid += 1
                if tokid >= len(tokens):
                    break
                token = tokens[tokid]
            if len(name):
                try:
                    mentions[(name, mid)] += [";".join(ids)]
                except:
                    mentions[(name, mid)] = [";".join(ids)]
            tokid += 1
        return mentions

    def get_one_features(self, data):
        X_train, y_train = [], []
        mention, mentions = data
        cands1 = self.generate_candidate(mention[0])
        # tic=time.time()
        # for eid in range(len(cands1)):
        #     features=cands1[eid]
        #     cmid=features[2]
        #     #features=features[3:]
        #     X_train+=[np.array(features)]
        #     y_train+=[np.array([int(cmid==mention[1])])]
        # return X_train,y_train
        # logger.info("Get cands:{} for {}".format(cands1,mention1))
        for c1 in cands1:
            meanwsrm, meanbigraph, maxwsrm, maxbigraph = 0, 0, [], []
            # sametype1=self.has_type(mention[0],c1[2])
            # if not sametype1:continue
            if c1[2] == "NIL":
                X_train += [np.array(c1 + [0] * 8)]
                y_train += [np.array([0])]
                continue
            try:
                vc1 = self.bigraph[c1[2]]
            except:
                pass
            # if c1[6]>3:continue

            for mention2 in mentions:
                if mention == mention2:
                    continue
                cands2 = self.generate_candidate(mention2[0])
                swsrm, sbigraph, mbigraph, mwsrm = 0, 0, 0, 0
                for c2 in cands2:
                    # sametype2=self.has_type(mention2[0],c2[2])
                    # if not sametype2:continue
                    # if c2[6]>3:continue
                    try:
                        vc2 = self.bigraph[c2[2]]
                    except:
                        pass
                    ## using bigraph method to compute entity cosine similarity ####
                    try:
                        bigraph = 1 - spatial.distance.cosine(vc1, vc2)
                    except:
                        bigraph = 0
                    # logger.info("Bigraph:{}for both {} and {} ".format(bigraph,c1[2],c2[2]))
                    sbigraph += bigraph
                    mbigraph = max(bigraph, mbigraph)
                    ### using wsrm method to compute entity relatedness ####
                    try:
                        sid, oid = self.wikidata_IDS[c1[2]], self.wikidata_IDS[c2[2]]
                    except:
                        pass  # logger.info("Problem with wikidata IDS:{}, {}".format(c1[2],c2[2]))
                    try:
                        t = self.wsrm[(sid, oid)]
                    except:
                        t = 0
                    try:
                        t += self.wsrm[(oid, sid)]
                    except:
                        pass
                    # if t:logger.info("WSRM:{:0.3f} for both {} and {} ".format(t,c1[2],c2[2]))
                    t = t / 2.0
                    swsrm += t
                    mwsrm = max(mwsrm, t)
                meanwsrm += swsrm
                meanbigraph += sbigraph
                maxwsrm += [mwsrm]
                maxbigraph += [mbigraph]
            maxa123p1 = heapq.nlargest(min(3, len(maxwsrm)), maxwsrm)
            if len(maxa123p1) < 3:
                maxa123p1 += (3 - len(maxa123p1)) * [0]
            maxa123pg = heapq.nlargest(min(3, len(maxbigraph)), maxbigraph)
            if len(maxa123pg) < 3:
                maxa123pg += (3 - len(maxa123pg)) * [0]
            X_train += [
                np.array(c1 + [meanwsrm] + maxa123p1 + [meanbigraph] + maxa123pg)
            ]
            y_train += [np.array([int(mention[1] == c1[2])])]
        # toc=time.time()-tic
        # logger.info("Computation time:{:0.3f}s".format(toc))
        return X_train, y_train

    def get_parallel_all_features(self, mentions, pools=10):
        X_train, y_train = [], []
        logger.info(
            "Starting generating features for {} mentions using {} pools".format(
                len(mentions), pools
            )
        )
        p = Pool(pools)
        data = [(mention, mentions) for mention in mentions]
        res = p.map(self.get_one_features, data)
        for r in res:
            tx, ty = r
            X_train += [tx]
            y_train += [ty]
        p.close()
        return X_train, y_train

    def get_all_features(self, mentions):
        doc = []
        X_train, y_train = [], []
        logger.info(
            "Starting generating features for {} mentions".format(len(mentions))
        )
        for mention1 in tqdm.tqdm(mentions):
            tx, ty = self.get_one_features((mention1, mentions))
            X_train += [tx]
            y_train += [ty]
        return X_train, y_train

    def get_cel_data(self, filename, start=0, end=151, test=False):
        logger.info("building files to train the classifier using wsrm")
        X_train, y_train = [], []
        ccr = ConllReader(filename).json
        if test:
            ccr = self.dev_file_conll
        i = 0
        if test:
            if "masked" in self.test_alis:
                end = 43
            else:
                end = 35
        logging.info("starting from {} to {}".format(start, end))
        features = {}
        oavgm = 0
        okdocs = 0
        if test:
            regex = self.path + "/data/bigFiles/{}.cel*".format(self.test_alis)
        else:
            regex = self.path + "/data/bigFiles/train.cel*"
        for file in tqdm.tqdm(glob(regex)):
            features.update(np.load(file, allow_pickle=True).item())
        print("size:{}".format(len(features)))
        for doc in tqdm.tqdm(ccr):
            logging.info("iteration:{}".format(i))
            if i >= start and i < end:
                tokens = doc["lis_tokens"]
                mentions = self.get_mentions(tokens, test=test)
                keys = list(mentions.keys())
                tx, ty = self.get_parallel_all_features(
                    keys, pools=max(multiprocessing.cpu_count(), self.args_dict.workers)
                )  # self.get_all_features(keys)#
                if len(mentions) != len(tx):
                    print(
                        "not the same=> x:{}, mentions:{}".format(
                            len(tx), len(mentions)
                        )
                    )
                avgm = 0
                nm = len(mentions)
                nonil = 0
                for c in range(nm):
                    features["mention_{}_doc_{}".format(c, i)] = (
                        keys[c],
                        np.array(mentions[keys[c]]),
                    )
                    features["x_{}_doc_{}".format(c, i)] = tx[c]
                    if not test:
                        features["y_{}_doc_{}".format(c, i)] = ty[c]
                        if keys[c][1] != "NIL":
                            nonil += 1
                            try:
                                avgm += sum(ty[c])[0]
                            except:
                                avgm += sum(ty[c])
                if not test and nonil:
                    oavgm += avgm / nonil
                    okdocs += 1
                    logging.info("Avg generation is {:0.3f}".format(oavgm / (okdocs)))

                if not test:
                    output = self.path + "data/bigFiles/train.cel.0-{}.fr".format(i)
                    loutput = self.path + "data/bigFiles/train.cel.0-{}.fr.npy".format(
                        i - 1
                    )
                else:
                    output = self.path + "data/bigFiles/{}.cel.0-{}.fr".format(
                        self.test_alis, i
                    )
                    loutput = self.path + "data/bigFiles/{}.cel.0-{}.fr.npy".format(
                        self.test_alis, i - 1
                    )
                np.save(output, features)
                if os.path.exists(loutput):
                    os.remove(loutput)
                    logging.info("Deleted file: {}".format(loutput))
                else:
                    logging.info("No file found {}".format(loutput))
                # print(features);exit()
            i += 1

        self.save()
        return np.array(X_train), np.array(y_train)

    def save(self):
        np.save(self.path + "data/bigFiles/cands.fr.npy", self.cands)
        # np.save(self.path+"data/bigFiles/types.fr.npy",self.types)
        # np.save(self.path+"data/bigFiles/name2wikidata.fr.npy",self.name2wikidata)

    def load(self):
        try:
            self.cands = np.load(
                self.path + "data/bigFiles/cands.fr.npy",
                allow_pickle=True,
                encoding="latin1",
            ).item()
        except:
            self.cands = {}
        # try:self.crosscands=np.load(self.path+"data/bigFiles/croswikidata.npy",allow_pickle=True,encoding="latin1").item()
        # except:self.crosscands={}
        # try:self.crossfr=np.load(self.path+"data/bigFiles/cands_cm.fr.npy",allow_pickle=True,encoding="latin1").item()
        # except:self.crossfr={}
        # try:self.name2wikidata=np.load(self.path+"data/bigFiles/name2wikidata.fr.npy",allow_pickle=True,encoding="latin1").item()
        # except:self.name2wikidata={}
        # try:self.wikipedi2wikidata=np.load(self.path+"data/bigFiles/wikidatawikipedia_cands.npy").item()
        # except:self.wikipedi2wikidata={}
        try:
            self.types = np.load(
                self.path + "data/bigFiles/types.fr.npy",
                allow_pickle=True,
                encoding="latin1",
            ).item()
        except:
            self.types = {}
        # try:self.bigraph=np.load(self.path+"data/bigFiles/wikidata_bigraph.entity.npy",allow_pickle=True,encoding="latin1").item()
        # except:self.bigraph={}
        if self.args_dict.wsrm:
            try:
                self.wikidata_IDS = np.load(
                    self.path + "data/bigFiles/wikidata_IDS.npy",
                    allow_pickle=True,
                    encoding="latin1",
                ).item()
            except:
                self.wikidata_IDS = {}
            try:
                self.wsrm = np.load(
                    self.path + "data/bigFiles/wsrm-wikidata.npy",
                    allow_pickle=True,
                    encoding="latin1",
                ).item()
            except:
                self.wsrm = {}
        self.cm = np.load(
            self.path + "data/bigFiles/cm.fr.npy", allow_pickle=True, encoding="latin1"
        ).item()
        self.exactLinker = np.load(
            self.path + "data/bigFiles/exactMatcher.npy",
            allow_pickle=True,
            encoding="latin1",
        ).item()
        try:
            self.wtranse = KeyedVectors.load_word2vec_format(
                self.args_dict.embed_file, binary=True, unicode_errors="ignore"
            )
        except:
            self.wtranse = FastText.load_fasttext_format(self.args_dict.embed_file)
        # logging.info("Found:{} cands, {} types, {} cm, {} exact match, {} wsrm, {} wsrm ID, {} bigraph".format(
        #     len(self.cands),len(self.types),len(self.cm),len(self.exactLinker),len(self.wsrm),len(self.wikidata_IDS),len(self.bigraph)))

    #####################
    # all folowing methods have been added from NELv2 Linker


from params import get_parser


def main():
    # Load parameters

    parser = get_parser()
    args_dict, unknown = parser.parse_known_args()

    opts = vars(args_dict)
    logging.info("------------ Options -------------")
    for k, v in sorted(opts.items()):
        logging.info("%s: %s" % (str(k), str(v)))
    logging.info("-----------------------------------")

    np.random.seed(args_dict.seed)
    linker = Linker(args_dict)
    # X,y=linker.get_data(linker.train_file)
    try:
        dev_file_conll = ConllReader(os.path.join(args_dict.path, args_dict.dev))
        linker.link_conll(dev_file_conll)
        linker.save()
    except Exception as e:
        linker.save()
        print(e)


if __name__ == "__main__":
    # make_crosswiki("../data/bigFiles/")
    main()
