#!/bin/python3

"""
took the baseline and (tried to) improve it
"""

import json
import logging
import random
import re
import os
import matplotlib.pyplot as plt
import pickle
import traceback
import sys
import warnings
from datetime import datetime

import numpy as np
import sklearn_crfsuite
from sklearn_crfsuite import metrics
from datetime import datetime
from pprint import pprint
from touch import touch
from tqdm import tqdm
from unidecode import unidecode

sys.path.append("CLEF-HIPE-2020-scorer/")
sys.path.append("/home/k/Documents/hipe2020/scripts/CLEF-HIPE-2020-scorer/")
sys.path.append(
    "/mnt/nfs/nas4/cicoda_tmp/cicoda_tmp/HIPE/hipe2020/scripts/CLEF-HIPE-2020-scorer"
)

from clef_evaluation import get_results_2
from features import word2features
from features import sent2features
from features import sent2labels
from hipe_utils import ConllReader
from hipe_utils import _output
from params import get_parser

logging.basicConfig(
    level=logging.DEBUG,
    format="[%(asctime)s] - [%(levelname)s] - %(message)s",
    handlers=[logging.FileHandler(__file__ + ".log"), logging.StreamHandler()],
)


logger = logging.getLogger(__name__)


class Hipe_CRF:
    def __init__(
        self, args,
    ):

        self.verbose_ = args.verbose
        self.lang = args.lang
        self.file_pred_name = "_tmp.tsv"
        touch(self.file_pred_name)
        os.remove(self.file_pred_name)

        # results baseline
        self.results_baseline = os.path.join(args.path, args.results_baseline)

        try:
            self.result_file = os.path.join(args.path, args.output_crf)
            touch(self.result_file)
        except (PermissionError, FileNotFoundError):
            self.result_file = os.path.join("..", args.output_crf)
            touch(self.result_file)

        self.fastText_vectors = args.fastText_vectors

        self.train_file_conll = ConllReader(os.path.join(args.path, args.train))
        self.dev_file_conll = ConllReader(os.path.join(args.path, args.dev))

        # reset the IOB in the dev file
        self.reset_entities_fieds_dev_file()

        self.original_dev_file_conll = ConllReader(os.path.join(args.path, args.dev))

        self.crf_config = {}
        # self.set_default_config()
        self.set_best_config()

        # loading fasttext vectors:
        if args.fastText_vectors:
            self.model = gensim.models.fasttext.load_facebook_model(
                "/nfs/nas4/cicoda_tmp/cicoda_tmp/HIPE/hipe2020/data/bigFiles/fr-model-skipgram-300minc20-ws5-maxn-6.bin"
            )

        self.output_crf = args.output_crf
        if args.addOtherFiles:
            self.train_file_conll.add_file(
                os.path.join(
                    args.path, "data/release-v01/fr/HIPE-data-v01-sample-fr.tsv"
                )
            )
            self.train_file_conll.add_file(
                os.path.join(
                    args.path, "data/training-v0.9/en/HIPE-data-v0.9-dev-en.tsv"
                )
            )

        if args.addDevFile:
            self.train_file_conll.add_file(
                os.path.join(
                    args.path, "data/training-v1.0/fr/HIPE-data-v1.0-dev-fr.tsv"
                )
            )

    def __str__(self):
        return f"""TRAIN:\t{self.train_file_conll}\nDEV:\t{self.dev_file_conll}"""

    __repr__ = __str__

    def __len__(self):
        return len(self.train_file_conll.json) + len(self.dev_file_conll.json)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        if exc_type:
            print(f"exc_type: {exc_type}")
            print(f"exc_value: {exc_value}")
            print(f"exc_traceback: {exc_traceback}")

    def get_FastText_vector(self, word):
        """return the FastTextVector associated to the word"""
        word = word.lower()
        try:
            vector = self.model[word][:100]
        except:
            # if the word is not in vocabulary,
            # returns zeros array
            vector = np.zeros(300,)[:100]
        return vector

    def reset_entities_fieds_dev_file(self, reset_char="_"):
        """rempalce all NE-COARSE-LIT , NEL etc by '_' """
        for doc in self.dev_file_conll:
            for tok in doc["lis_tokens"]:
                for key in tok:
                    if key in ("TOKEN", "MISC", "IS_WORD", "POSITION"):
                        continue
                    tok[key] = reset_char

    def create_datasets(self, col_name="NE-COARSE-LIT", num=100000):
        """create the associated data set (eg classic X_train, X_test, ..."""
        # train and test sentences:
        self.train_sents, self.test_sents = [], []
        for doc in self.train_file_conll.json[:num]:
            _ls = []
            for tok in doc["lis_tokens"]:
                if tok["IS_WORD"]:
                    if self.fastText_vectors:
                        _ls.append(
                            (
                                tok["TOKEN"],
                                self.get_FastText_vector(tok["TOKEN"]),
                                tok[col_name],
                            )
                        )
                    else:
                        _ls.append((tok["TOKEN"], "", tok[col_name]))

            self.train_sents.append(_ls)
        for doc in self.dev_file_conll.json:
            _ls = []
            for tok in doc["lis_tokens"]:
                if tok["IS_WORD"]:
                    if self.fastText_vectors:
                        _ls.append(
                            (
                                tok["TOKEN"],
                                self.get_FastText_vector(tok["TOKEN"]),
                                tok[col_name],
                            )
                        )
                    else:
                        _ls.append((tok["TOKEN"], "", tok[col_name],))
            self.test_sents.append(_ls)

        self.X_train = [
            sent2features(s, middle_col=self.fastText_vectors) for s in self.train_sents
        ]
        self.y_train = [sent2labels(s) for s in self.train_sents]
        assert len(self.X_train) == len(self.y_train) == len(self.train_file_conll)
        for x, y in zip(self.X_train, self.y_train):
            assert len(x) == len(y)

        self.X_test = [
            sent2features(s, middle_col=self.fastText_vectors) for s in self.test_sents
        ]
        self.y_test = [sent2labels(s) for s in self.test_sents]
        assert len(self.X_test) == len(self.y_test) == len(self.dev_file_conll)
        for x, y in zip(self.X_test, self.y_test):
            assert len(x) == len(y)

    def verbose(self, arg):
        """the verbose function"""
        if self.verbose_:
            try:
                logger.debug(json.dumps(arg, indent=4, sort_keys=True))
            except TypeError:
                logger.debug(arg)

    def set_default_config(self, dic={}):
        if not self.crf_config and not dic:
            dic = {
                "algorithm": "lbfgs",
                "c1": 0.2,
                "c2": 0.1,
                "min_freq": None,
                "max_iterations": 199,
                "all_possible_transitions": True,
                "num_memories": None,
                "epsilon": None,
                "period": None,
                "delta": None,
                "linesearch": None,
                "max_linesearch": None,
            }
        self.crf_config.update(dic)

    def set_best_config(self):
        dic = {
            "algorithm": "lbfgs",
            "c1": 0.17978644989356052,
            "c2": 0.05506289813560372,
            "min_freq": 0,
            "max_iterations": 192,
            "all_possible_transitions": False,
            "num_memories": 4,
            "epsilon": 1e-05,
            "period": None,
            "delta": 0.0001,
            "linesearch": None,
            "max_linesearch": None,
        }
        self.crf_config.update(dic)

    def set_random_config(self):
        """set a random configuration of the crf dict parameters"""
        self.crf_config[
            "algorithm"
        ] = "lbfgs"  # random.choice(['lbfgs',  'l2sgd', 'ap', 'pa', 'arow'])
        self.crf_config["min_freq"] = random.random()
        self.crf_config["all_possible_states"] = random.choice(
            [True, False]
        )  # bool, optional
        self.crf_config["c1"] = random.random()  # random.choice([0, 0.1, 0.2]), #float
        self.crf_config[
            "c2"
        ] = random.random()  # random.choice([0.1, 0.5, 1.0]), #float
        self.crf_config["max_iterations"] = random.choice(
            [None, random.randint(100, 1000)]
        )  # (int, optional (default=None)) –

        self.crf_config["num_memories"] = random.choice(
            [None, random.randint(4, 8)]
        )  # (int, optional (default=6)) –
        self.crf_config["epsilon"] = random.choice(
            [None, random.choice([1e-4, 1e-5, 1e-5])]
        )  # (float, optional (default=1e-5)) –
        self.crf_config["period"] = random.choice(
            [None, random.randint(5, 20)]
        )  # (int, optional (default=10)) –
        self.crf_config["delta"] = random.choice(
            [None, random.choice([1e-4, 1e-5, 1e-5])]
        )  # (float, optional (default=1e-5)) –
        self.crf_config["linesearch"] = random.choice(
            [None, random.choice(["MoreThuente", "Backtracking", "StrongBacktracking"])]
        )
        self.crf_config["max_linesearch"] = random.choice(
            [None, random.randint(10, 30)]
        )  # (int, optional (default=20)) –

        self.crf_config["all_possible_transitions"] = random.choice([True, False])

    def train_predict(
        self, col_name="NE-COARSE-LIT", num_dataset=1000, hand_made_rules=False,
    ):
        """train a crf model on the X_train... attributes
        and update the json file """

        self.verbose("creating the dataset")
        self.create_datasets(col_name=col_name)
        self.verbose("init crf config")

        self.crf = sklearn_crfsuite.CRF(
            algorithm=self.crf_config["algorithm"],
            c1=self.crf_config["c1"],
            c2=self.crf_config["c2"],
            max_iterations=self.crf_config["max_iterations"],
            all_possible_transitions=self.crf_config["all_possible_transitions"],
            num_memories=self.crf_config["num_memories"],
            epsilon=self.crf_config["epsilon"],
            period=self.crf_config["period"],
            delta=self.crf_config["delta"],
            linesearch=self.crf_config["linesearch"],
            max_linesearch=self.crf_config["max_linesearch"],
            verbose=self.verbose_,
        )
        self.verbose(self.crf)
        self.verbose("fit crf")

        self.crf.fit(self.X_train, self.y_train)

        self.verbose("done")
        # labels = list(crf.classes_)
        # labels.remove('O')
        y_pred = self.crf.predict(self.X_test)
        self.y_pred = y_pred

        if hand_made_rules:
            self.perform_rules()

        self._update_lis_tokens(y_pred, col_name)

        # self.correct_bio()

    def _update_lis_tokens(self, y_pred, col_name):
        """update the list of tokens with predictions"""
        try:
            assert len(self.dev_file_conll.json) == len(y_pred), (
                len(self.dev_file_conll.json),
                len(y_pred),
            )
        except AssertionError:
            warnings.warn(
                f"error while asserting len(self.dev_file_conll.json) == len(y_pred)"
            )
        for doc, pred in zip(self.dev_file_conll.json, y_pred):
            try:
                assert len([tok for tok in doc["lis_tokens"] if tok["IS_WORD"]]) == len(
                    pred
                ), (
                    len([tok for tok in doc["lis_tokens"] if tok["IS_WORD"]]),
                    len(pred),
                )
            except AssertionError:
                warnings.warn(
                    f"error while asserting len([tok for tok in doc['lis_tokens'] if tok['IS_WORD']]) == len(pred)"
                )
            i = 0
            for tok in doc["lis_tokens"]:
                if tok["IS_WORD"]:
                    tok[col_name] = pred[i]
                    i += 1

    def create_data_train_predict_date(self, col_name="NE-COARSE-LIT", range_=100):
        """create the associated dataset with a a date, and train the model with it"""
        ls_doc_train = []
        ls_doc_test = []
        dates = []
        for temp in range(1790, 2020, range_):
            dates.append((str(temp), str(temp + range_)))
        self.verbose(dates)

        for date in dates:
            ls_doc_train = [
                doc
                for doc in self.train_file_conll
                if date[0] < doc["date"] < date[1] and doc not in ls_doc_train
            ]
            ls_doc_test = [
                doc
                for doc in self.dev_file_conll
                if date[0] < doc["date"] < date[1] and doc not in ls_doc_test
            ]

            self.verbose([doc["date"] for doc in ls_doc_train])
            self.verbose([doc["date"] for doc in ls_doc_test])
            assert ls_doc_train
            assert ls_doc_test
            # create sentences
            train_sents, test_sents = [], []
            self.verbose(f"""{len(ls_doc_train)}\ttraining\n{len(ls_doc_test)}\ttest""")
            for doc in ls_doc_train:
                _ls = []
                for tok in doc["lis_tokens"]:
                    if tok["IS_WORD"]:
                        _ls.append((tok["TOKEN"], "___", tok[col_name]))

                train_sents.append(_ls)
            for doc in ls_doc_test:
                _ls = []
                for tok in doc["lis_tokens"]:
                    if tok["IS_WORD"]:
                        _ls.append((tok["TOKEN"], "___", tok[col_name],))
                test_sents.append(_ls)

            X_train = [sent2features(s) for s in train_sents]
            y_train = [sent2labels(s) for s in train_sents]
            assert len(X_train) == len(y_train)  # == len(train_file)
            for x, y in zip(X_train, y_train):
                assert len(x) == len(y)
            X_test = [sent2features(s) for s in test_sents]
            y_test = [sent2labels(s) for s in test_sents]
            assert len(X_test) == len(y_test)
            for x, y in zip(X_test, y_test):
                assert len(x) == len(y)

            assert len(X_train) == len(y_train) == len(ls_doc_train)
            assert len(X_test) == len(y_test) == len(ls_doc_test)

            # train and predict with model

            self.verbose("init crf config")
            self.crf = sklearn_crfsuite.CRF(
                algorithm="lbfgs",
                c1=0.2,
                c2=0.1,
                max_iterations=199,
                all_possible_transitions=True,
            )
            self.verbose(self.crf)
            self.verbose("fit crf")
            self.crf.fit(X_train, y_train)
            self.verbose("done")

            y_pred_all = self.crf.predict(X_test)

            # update the list of tokens
            for doc in self.dev_file_conll:
                for y_pred, doc_updated in zip(y_pred_all, ls_doc_test):
                    if doc["date"] == doc_updated["date"]:
                        assert doc["document_id"] == doc_updated["document_id"]
                        i = 0
                        for tok in doc["lis_tokens"]:
                            if tok["IS_WORD"]:
                                tok[col_name] = y_pred[i]
                                i += 1
        self.dev_file_conll.to_conll(self.file_pred_name)

    def perform_rules(self):
        """perform handmade rules on the y_pred"""
        ls_towns = [
            unidecode(line.lower().strip())
            for line in open("../ressources/ls_towns_fr.txt").read().splitlines()
        ]
        ls_countries = [
            unidecode(line.lower().strip())
            for line in open("../ressources/ls_country_fr.txt").read().splitlines()
        ]
        for i, (x_sent, y_sent) in enumerate(zip(self.X_test, self.y_pred)):
            for j, (x_word, y_word) in enumerate(zip(x_sent, y_sent)):
                word = unidecode(x_word["word.lower()"])
                # country name:
                if word in ls_countries and y_word == "O":
                    self.y_pred[i][j] = "B-LOC"
                if word in ls_towns and y_word == "O":
                    self.y_pred[i][j] = "B-LOC"

    def get_results(self):
        """get the results and the config file for the crf"""
        self.dev_file_conll.to_conll(self.file_pred_name)
        logger.info("official file is " + self.dev_file_conll.conll_file_name[0])
        logger.info("predictions file is " + self.file_pred_name)
        self.results = get_results_2(
            self.dev_file_conll.conll_file_name[0],
            self.file_pred_name,
            "nerc_coarse",
            skip_check=True,
        )

    def output_wrong_prediction(self, out_file=None):
        """return the wrong prediction in a tsv format (easier to read)"""
        res = str(datetime.now()) + "\n"
        res += "i\tj\tTOKEN\tTRUE\tPRED\n"
        for i, (doc_, tok_, true_, pred_) in enumerate(
            zip(self.dev_file_conll, self.X_test, self.y_test, self.y_pred)
        ):
            res += f"""\n# document_id = {doc_['document_id']}\n"""
            res += f"""# language = {doc_['language']}\n"""
            k = 0
            for j, (tok, true, pred) in enumerate(zip(tok_, true_, pred_)):
                if true != pred:
                    if j != k + 1:
                        res += "\n"
                    res += f"""{i}\t{j}\t{tok['word.lower()']}\t{true}\t{pred}\n"""
                    k = j
        _output(res, out_file)

    def grid_search(self):
        pass

    def random_search(
        self,
        num_tests=0,
        res_file="../results/results_RS_2020_04_03.tsv",
        all_res="../results/all_RS.json",
    ):
        """perform a random search of the parameters
        if num_tests == 0 : will perfom the search forever"""
        i = 1
        while i != num_tests:
            i += 1
            print(f"""test\t{i}""")
            try:
                self.set_random_config()
                self.train_predict()
                self.get_results()
                print(
                    self.results["NE-COARSE-LIT"]["ALL"]["strict"]["F1_micro"],
                    self.results["NE-COARSE-LIT"]["ALL"]["ent_type"]["F1_micro"],
                    self.crf_config,
                    sep="\t",
                    file=open(res_file, "a"),
                )
                ls_res = json.load(open(all_res))
                ls_res.append({"parameters": self.crf_config, "results": self.results})
                json.dump(ls_res, open(all_res, "w"), indent=4, sort_keys=True)
            except Exception as e:
                print(e)

    def plot_learning_rate(self, start=10, picture_name="joli_learning_rate.jpg"):
        """plot the learning rate of the crf: useful to know if giving more data would help"""
        touch(picture_name)
        ls_results = []
        ls_x = []
        for i in tqdm(range(start, len(self.train_file_conll.json))):
            ls_x.append(i)
            self.train_predict(num_dataset=i)
            self.get_results()
            ls_results.append(self.results)
            pickle.dump(ls_results, open("../results/_tmp_learning_rate.pkl", "bw"))

        nums_1 = [
            results["NE-COARSE-LIT"]["ALL"]["strict"]["F1_micro"]
            for results in ls_results
        ]
        nums_2 = [
            results["NE-COARSE-LIT"]["ALL"]["ent_type"]["F1_micro"]
            for results in ls_results
        ]
        nums_3 = [
            results["NE-COARSE-LIT"]["ALL"]["ent_type"]["F1_macro"]
            for results in ls_results
        ]
        nums_4 = [
            results["NE-COARSE-LIT"]["ALL"]["strict"]["F1_macro"]
            for results in ls_results
        ]

        plt.plot(ls_x, nums_1, label="['NE-COARSE-LIT']['ALL']['strict']['F1_micro']")
        plt.plot(ls_x, nums_2, label="['NE-COARSE-LIT']['ALL']['ent_type']['F1_micro']")
        plt.plot(ls_x, nums_3, label="['NE-COARSE-LIT']['ALL']['ent_type']['F1_macro']")
        plt.plot(ls_x, nums_4, label="['NE-COARSE-LIT']['ALL']['strict']['F1_macro']")

        plt.xticks([x for x in ls_x if not x % 5], rotation="90")
        plt.xlabel("Nbr of Docs")
        plt.ylabel("Scores")
        plt.legend(loc="lower right")
        plt.title("Learning rate")
        plt.grid(True)
        plt.savefig(picture_name, dpi=300)

    def print_results(self):
        """print the result on standart output"""
        self.get_results()
        print(self.results["NE-COARSE-LIT"]["ALL"]["strict"]["F1_micro"])
        print(self.results["NE-COARSE-LIT"]["ALL"]["ent_type"]["F1_micro"])


def main():
    logger.info("__________________________")
    logger.info(__file__)
    logger.info(os.getpid())

    parser = get_parser()
    args_dict, unknown = parser.parse_known_args()

    with Hipe_CRF(args_dict) as h_crf:
        h_crf.train_file_conll.add_file(
            "../data/release-v01/fr/HIPE-data-v01-sample-fr.tsv"
        )
        h_crf.train_file_conll.add_file(
            "../data/training-v0.9/en/HIPE-data-v0.9-dev-en.tsv"
        )

        logger.debug(h_crf)

        h_crf.set_best_config()
        logger.debug("train and predict")
        h_crf.train_predict()
        # h_crf.train_predict(hand_made_rules=True)

        logger.debug("getting the results")
        h_crf.get_results()

        logger.debug(h_crf.results["NE-COARSE-LIT"]["ALL"]["strict"]["F1_micro"])
        logger.debug(h_crf.results["NE-COARSE-LIT"]["ALL"]["ent_type"]["F1_micro"])

        bad_res = "_mauvais_results.tsv"
        logger.debug(f"bad res: {bad_res}")
        h_crf.output_wrong_prediction(bad_res)


if __name__ == "__main__":
    main()
