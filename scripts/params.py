import argparse


def get_parser():
    parser = argparse.ArgumentParser()

    # Files
    parser.add_argument(
        "--path",
        default="/nfs/nas4/cicoda_tmp/cicoda_tmp/HIPE/hipe2020/",
        help="Absolute pth to data",
    )
    parser.add_argument(
        "--train",
        default="data/training-v1.0/fr/HIPE-data-v1.0-train-fr.tsv",
        help="Training set data file",
    )
    # parser.add_argument('--train', default='data/training-v1.1/fr/HIPE-data-v1.1-train-fr.tsv', help='Training set data file')
    parser.add_argument("--lang", default="fr", help="The used language")
    # parser.add_argument('--neldev', default='results/fr/irisa-nel-nlp_bundle2_fr_1.tsv', help='Dev set data file')
    parser.add_argument(
        "--dev",
        default="data/training-v1.0/fr/HIPE-data-v1.0-dev-fr.tsv",
        help="Dev set data file",
    )
    parser.add_argument(
        "--output",
        default="results/fr/irisa-ner-crf-nelv2-topk_bundle2_fr_1.tsv",
        help="Dataset test data file",
    )
    parser.add_argument(
        "--output_crf",
        default="results/fr/irisa-nel-nlp_bundle2_fr_1.tsv",
        help="Dataset test data file",
    )
    parser.add_argument(
        "--results_baseline",
        default="results/results_nerc_coarse_LANG_all_baseline.json",
        help="the result file of the baseline",
    )

    # Training opts classifier
    parser.add_argument("--bigraph", default=False, type=bool, help="use bigraph model")
    parser.add_argument("--wsrm", default=False, type=bool, help="use WSRM model")
    parser.add_argument("--classifier", default="MLP", type=str)
    parser.add_argument("--features", default="", type=str)
    parser.add_argument("--seed", default=1234, type=int)
    parser.add_argument("--workers", default=16, type=int)
    parser.add_argument("--topk", default=1, type=int)
    parser.add_argument(
        "--embed_file",
        default="/nfs/nas4/cicoda_tmp/cicoda_tmp/word2vec/trunk/models/skipgram.s100.w5.enwikianchor1G.bin",
        help="Word2vec embedding file",
    )
    parser.add_argument(
        "--bigraph_file",
        default="data/bigFiles/wikidata_bigraph.entity.npy",
        help="Word2vec embedding file",
    )
    parser.add_argument(
        "--wsrm_dir",
        default="data/bigFiles/",
        help="Path contains two dics: WSRM values and entity ids",
    )
    parser.add_argument(
        "--baseline", default="dic_wiki", type=str, help="The simple baseline "
    )
    parser.add_argument("--start", default=0, type=int)
    parser.add_argument("--end", default=150, type=int)

    # Bools
    parser.add_argument(
        "--verbose", dest="verbose", action="store_true", help="verbose"
    )
    parser.add_argument(
        "--useFastText",
        dest="fastText_vectors",
        action="store_true",
        help="use fastTextVectors",
    )

    parser.add_argument(
        "--addOtherFiles",
        dest="otherFiles",
        action="store_true",
        help="add the other files",
    )
    parser.add_argument(
        "--addDevFile",
        dest="addDevFile",
        action="store_true",
        help="add the dev file to the train",
    )

    # set defaults:
    parser.set_defaults(verbose=False)
    parser.set_defaults(fastText_vectors=False)
    parser.set_defaults(addOtherFiles=False)
    parser.set_defaults(addDevFile=False)
    return parser
