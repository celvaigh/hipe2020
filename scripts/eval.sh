path=/mnt/nfs/nas4/cicoda_tmp/cicoda_tmp/HIPE/hipe2020/
word2vec=/mnt/nfs/nas4/cicoda_tmp/cicoda_tmp/word2vec/trunk/models/skipgram.s100.w5.enwikianchor1G.bin
fasttext=$path/data/bigFiles/fr-model-skipgram-300minc20-ws5-maxn-6.bin
testpath=data/test-masked-v1.2/fr/HIPE-data-v1.2-test-masked-fr.tsv
out1=results/fr/irisa-ner-crf-nelv2_bundle2_fr_1.tsv
# for c in REG GB QDA ADAB
# do
#pipeline
#NELv2
#CEL

python3 pipeline.py --classifier MLP --path $path --embed_file $word2vec --dev $testpath --output $outpath
#--features 0,1,3
#--eval data/training-v1.0/fr/HIPE-data-v1.1-dev-fr.tsv
#11,12,13,14,7,8,9,10