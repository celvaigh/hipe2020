import matplotlib

matplotlib.use("Agg")
from glob import glob
import numpy as np
import tqdm
import matplotlib.pyplot as plt


def fuzion(args_dic, test=True):
    if test:
        regex = args_dic.path + "/data/bigFiles/test-masked.cel*"
    else:
        regex = args_dic.path + "/data/bigFiles/train.cel*"
    X, y = [], []
    dic = {}
    for file in tqdm.tqdm(glob(regex)):
        dic.update(np.load(file, allow_pickle=True).item())
    print("train size:{}".format(len(dic)))
    for m in tqdm.tqdm(dic):
        if "mention_" not in m:
            continue
        try:
            x = dic[m.replace("mention_", "x_")]
            if not test:
                yt = dic[m.replace("mention_", "y_")]
            # else:yt=[int(i[2]==dic[m][1])for i in x]
            if (not test and not len(yt)) or not len(x):
                continue
            # print("sample=> mention:{}, y:{}".format(dic[m],y))
            X += [np.array(i[3:]).astype(float) for i in x]
            if not test:
                y += yt
        except Exception as e:
            print(e)  # ;exit()
    return np.array(X), np.array(y), dic


# pip install umap-learn
def hdbscan_plot(output):
    import umap.umap_ as UMAP

    # import hdbscan
    test = False
    X, y, _ = fuzion(test=test)
    # X=[i[3:] for i in X]
    print(X.shape, y.shape)
    print("vectors:{}, sample:{}, dim:{}".format(len(X), X[0], len(X[0])))
    model = UMAP.UMAP(
        n_neighbors=25, min_dist=0.0, n_components=3, a=1.5, b=2, verbose=True
    )
    p_umap = model.fit_transform(X)
    # clusterer = hdbscan.HDBSCAN(min_cluster_size=2).fit(p_umap)
    if not test:
        colors = [["blue", "red"][int(i[0])] for i in y]
    else:
        colors = None
    plt.figure(figsize=(10, 10))
    plt.scatter(
        p_umap[:, 0], p_umap[:, 1], c=colors, edgecolors="gray", s=30, cmap="Set1"
    )
    plt.axis("off")
    if test:
        plt.savefig(output + "umap_hdbscan_test.png")
    else:
        plt.savefig(output + "umap_hdbscan.png")


from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.decomposition import PCA
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.neighbors import KNeighborsClassifier, NeighborhoodComponentsAnalysis
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler


def pca(output):
    n_neighbors = 3
    random_state = 0
    X, y, _ = fuzion(test=False)
    X = np.array([np.array(i) for i in X])
    print("vectors:{}, sample:{}, dim:{}".format(len(X), X[0], len(X[0])))
    print(X.shape, y.shape)
    # print(X[:10])
    # print(y[:10]);exit()
    # Split into train/test
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.1, stratify=y, random_state=random_state
    )
    dim = len(X[0])
    n_classes = len(np.unique(y))
    # Reduce dimension to 2 with PCA
    pca = make_pipeline(
        StandardScaler(), PCA(n_components=2, random_state=random_state)
    )
    # Reduce dimension to 2 with LinearDiscriminantAnalysis
    lda = make_pipeline(StandardScaler(), LinearDiscriminantAnalysis(n_components=2))
    # Reduce dimension to 2 with NeighborhoodComponentAnalysis
    nca = make_pipeline(
        StandardScaler(),
        NeighborhoodComponentsAnalysis(n_components=2, random_state=random_state),
    )
    # Use a nearest neighbor classifier to evaluate the methods
    knn = KNeighborsClassifier(n_neighbors=n_neighbors)
    # Make a list of the methods to be compared
    dim_reduction_methods = [("PCA", pca)]  # , ('LDA', lda), ('NCA', nca)]

    # plt.figure()
    for i, (name, model) in enumerate(dim_reduction_methods):
        plt.figure()
        # plt.subplot(1, 3, i + 1, aspect=1)
        # Fit the method's model
        model.fit(X_train, y_train)
        # Fit a nearest neighbor classifier on the embedded training set
        knn.fit(model.transform(X_train), y_train)
        # Compute the nearest neighbor accuracy on the embedded test set
        acc_knn = knn.score(model.transform(X_test), y_test)
        # Embed the data set in 2 dimensions using the fitted model
        X_embedded = model.transform(X)
        colors = [["blue", "red"][int(i[0])] for i in y]
        # Plot the projected points and show the evaluation score
        plt.scatter(X_embedded[:, 0], X_embedded[:, 1], c=colors, s=30, cmap="Set1")
        plt.title(
            "{}, KNN (k={})\nTest accuracy = {:.2f}".format(name, n_neighbors, acc_knn)
        )
    plt.axis("off")
    plt.savefig(output + "pca_knn{}.".format(n_neighbors))


def main():
    # fuzion()
    hdbscan_plot("/mnt/nfs/nas4/cicoda_tmp/cicoda_tmp/HIPE/hipe2020/data/output/fr/")
    # pca("/mnt/nfs/nas4/cicoda_tmp/cicoda_tmp/HIPE/hipe2020/data/output/fr/")


if __name__ == "__main__":
    main()
