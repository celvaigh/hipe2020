#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import requests, json, sys
import numpy as np
import pandas as pd
import tqdm
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score

from hipe_utils import ConllReader


def getWikipediaIdWikidataId(lang, name):
    url = "https://{}.wikipedia.org/w/api.php?action=query&prop=pageprops&titles={}&format=json".format(
        lang, "_".join(str(name).split(" "))
    )
    try:
        res = json.loads(requests.get(url).text)
        wpid = list(res["query"]["pages"].keys())[0]
        wdid = res["query"]["pages"][wpid]["pageprops"]["wikibase_item"]
        return (wpid, wdid)
    except:
        return None


class Tokenizer:
    def __init__(self):
        self.words = ""
        self.start = 0
        self.end = 0
        self.type = ""

    def __str__(self):
        return "{} {} {} {}".format(self.words, self.type, self.start, self.end)


class Linker:
    def __init__(self, lang, filename, output):
        self.name = "Linker0"
        self.lang = lang
        self.filename = filename
        self.data = []
        self.docs = []
        self.linked = []
        self.history = {}
        self.output = output
        self.link_coll()  # read_data()
        # self.link()

    def link_entity(self, name):
        name = name.capitalize()
        try:
            mid = self.history[name]
        except:
            found = getWikipediaIdWikidataId(self.lang, name)
            if found:
                mid = found[1]
            else:
                mid = "NIL"
            self.history[name] = mid
        return mid

    def link_coll(self):
        self.cr = ConllReader(self.filename)
        ccr = self.cr.json
        doci = 0
        for doc in tqdm.tqdm(ccr):
            # for token in doc["lis_tokens"]:print(token["TOKEN"],token["NEL-LIT"],token["NE-COARSE-LIT"])
            # print("##################################################################################")
            name = ""
            tmpdoc = doc
            tokid = 0
            # print(len(doc["lis_tokens"]))
            for token in doc["lis_tokens"]:

                if token["NE-COARSE-LIT"].startswith("B-"):
                    name = token["TOKEN"]
                    ids = [token["POSITION"]]
                elif token["NE-COARSE-LIT"].startswith("I-"):
                    name += " " + token["TOKEN"]
                    ids += [token["POSITION"]]
                elif name != "":
                    mid = self.link_entity(name)
                    l = len(name.split(" "))
                    # print(l,tokid)
                    for idx in range(tokid - l, tokid):
                        try:
                            tmpdoc["lis_tokens"][idx]["NEL-LIT"] = mid
                        except:
                            print(l, tokid)
                            exit()
                        # if "M ." in name:print(self.cr.json[doci]["lis_tokens"][idx]["NEL-LIT"])
                    name = ""
                    ids = []
                tokid += 1
            # print(doc["document_id"])
            self.cr.json[doci] = tmpdoc
            doci += 1
            # for token in self.cr.json[doci-1]["lis_tokens"]:print(token["TOKEN"],token["NEL-LIT"],token["NE-COARSE-LIT"])
            # exit()
        self.cr.to_conll(self.output)

    def read_data(self):
        self.data = pd.read_csv(self.filename, sep="\t")
        mentions = [str(i) for i in self.data["TOKEN"]]
        typ = [str(i) for i in self.data["NE-COARSE-LIT"]]
        MISC = [str(i) for i in self.data["MISC"]]
        i = 0
        tokens = []
        while i < len(mentions):
            if "# document_id" in mentions[i]:
                if len(tokens):
                    self.docs += [tokens]
                token = Tokenizer()
            if "#" in mentions[i]:
                i += 1
                continue
            j = i + 1

            if typ[i] != "nan" and typ[i].startswith("B-"):
                token.start = i
                token.words = mentions[i] + " " * int(MISC[i] == "_")
                token.type = typ[i][2:]
            while j < len(mentions) - 1 and typ[j] != "nan" and typ[j].startswith("I-"):
                token.words += mentions[j] + " " * int(MISC[j - 1] == "_")
                j += 1

            token.end = j
            if token.start != 0:
                tokens += [token]
                token = Tokenizer()
            i = j
        self.docs += [tokens]
        for i in range(len(mentions)):
            if "#" in str(self.data["TOKEN"][i]):
                self.linked += [None]
            else:
                self.linked += ["_"]
        print("collection got {} docs".format(len(self.docs)))

    def link(self):
        print("linking file:{}".format(self.filename))
        for tokens in tqdm.tqdm(self.docs):
            self.link_doc(tokens)
        self.data[self.name] = self.linked
        self.data.to_csv(self.output, sep="\t", index=False)
        print("linking finished")

    def link_doc(self, doc):

        for token in doc:
            name = token.words.capitalize()
            try:
                mid = self.history[name]
            except:
                found = getWikipediaIdWikidataId(self.lang, name)
                if found:
                    mid = found[1]
                else:
                    mid = "NIL"
                self.history[name] = mid
            for i in range(token.start, token.end):
                if self.linked[i]:
                    self.linked[i] = mid

    def eval(self):
        try:
            self.data = pd.read_csv(self.output, sep="\t")
        except:
            self.link()
        true, pred = self.data["NEL-LIT"].astype(str), self.data[self.name].astype(str)
        y_true, y_pred = [], []
        assert len(true) == len(pred)
        for i in range(len(true)):
            if true[i] != "_":
                y_true += [true[i]]
                y_pred += [pred[i]]
        print(
            "Precision:{}, Recall:{}, F1-micro:{}".format(
                precision_score(y_true, y_pred, average="micro"),
                recall_score(y_true, y_pred, average="micro"),
                f1_score(y_true, y_pred, average="micro"),
            )
        )


def main():
    if len(sys.argv) == 1:
        linker = Linker(
            "fr",
            "../data/release-v01/fr/HIPE-data-v01-sample-fr.tsv",
            "../results/fr/HIPE-data-v01-sample-fr.tsv",
        )
        linker.eval()
    elif len(sys.argv) == 3:
        inpufile, outputfile = sys.argv[1], sys.argv[2]
        if "fr" in inpufile:
            lang = "fr"
        elif "de" in inpufile:
            lang = "de"
        elif "en" in inpufile:
            lang = "en"
        else:
            print("unknown lang from file {}".format(inpufile))
        linker = Linker(lang, inpufile, outputfile)
        # linker.eval()


if __name__ == "__main__":
    main()
