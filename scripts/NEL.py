#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# oarsub -I -l /cpu=2,walltime=2:00:00 -p "mem_cpu>='48179'"
import heapq
import os, sys, time, math
import numpy as np
import pandas as pd
from gensim.models import KeyedVectors
from scipy import spatial
import bz2
import re
import string
import operator
from itertools import groupby
import itertools
import wikipedia
import tqdm
from multiprocessing import Pool

# import hipe_utils as ut
def updateabrdic():
    with open("/nfs/nas4/cicoda_tmp/cicoda_tmp/entitylinking/accronymes.csv") as f:
        tmp = f.read().split("\n")
    abrdic = {}
    for i in tmp:
        j = i.split(";")
        abrdic[j[0]] = j[1:]
    return abrdic


def getAccro(key, idx):
    try:
        return abrdic[key.lower()][idx].lower()
    except:
        return key


punc = string.punctuation + " "


def remove_punc(s, punc):
    return s.lower().translate(string.maketrans("", ""), punc)


def get_article(aid):
    return wikipedia.page(pageid=aid).title


def get_article_id(title):
    try:
        return wikipedia.page(title).pageid
    except:
        return None


def wdata2wpedia(filename):
    s = time.time()
    data = pd.read_csv(filename, sep="\t")
    wpedia = data["wpedia"]
    wdata = data["wdata"]
    wd2wp = {}
    p = Pool(20)
    res = p.map(get_article_id, wpedia)
    p.terminate()
    p.join()
    print("tooks : {}".format(time.time() - s))
    for i in tqdm.tqdm(range(len(res))):
        if res[i] != None:
            wd2wp[wdata[i]] = res[i]
    print("saved : {}".format(len(wd2wp)))
    np.save(
        "/nfs/nas4/cicoda_tmp/cicoda_tmp/HIPE/hipe2020/data/bigFiles/wd2wp.npy", wd2wp
    )


def convertType(typ):
    if typ == "PER":
        typ = "f_people.person"
    elif typ == "GPE" or typ == "LOC" or typ == "FAC":
        typ = "f_location.location"
    elif typ == "ORG":
        typ = "f_organization.organization"
    return typ


freebasenames = {}
freebasetype = {}
wikipages = {}
wikisearch = {}
generatedcands = {}
freebasetype = {}


def generate_candidate(mention):
    global freebasenames
    global wikipages
    global generatedcands
    es, ps = [], []
    try:
        return generatedcands[mention]
    except:
        pass
    try:
        es, ps = crosswiki[
            remove_punc(getAccro(mention, 0), punc)
        ]  # candidates generation from crosswiki
    except:
        # Mention not in crosswiki use wikipedia then
        try:
            wcands = wikisearch[getAccro(mention, 0)]
        except:
            try:
                wcands = wikipedia.search(getAccro(mention, 0))
            except:
                generatedcands[mention] = [es, ps]
                return [es, ps]
            wikisearch[getAccro(mention, 0)] = wcands
        for wcand in wcands:
            try:
                wid = wikipages[wcand]
            except:
                try:
                    wid = wikipedia.page(wcand).pageid
                except:
                    continue  # wid="NIL"
                wikipages[wcand] = wid
                try:
                    p = cm[(mention.lower(), wcand.lower())]
                except:
                    p = 0
                ps += [p]
                es += [wid]
            try:
                freebasenames[wids2mids[wid]] = wcand
            except:
                pass
    cand = [es, ps]
    generatedcands[mention] = cand
    return cand


def make_mention(sentence, entityRel):
    doci = 0
    doc = []
    mentions = []
    dicmentions = {}
    global freebasenames
    global freebasetype
    goodgen = 0
    while True:
        if doci >= len(sentence) or len(sentence) < 1:
            break
        s1 = sentence[doci]
        # print(doci)
        mention, mid, typ = s1[0], s1[2], s1[3]
        try:
            doc += [dicmentions[mid]]
            doci += 1
            continue
        except:
            pass
        cand = generate_candidate(mention)  # generate_candidate(mention)
        mids = cand[0]
        mentions = []
        # if len(cand[0])==0:mentions=[[mention,"NIL","NIL","NIL",0,0,0,0,0,0,0,0]]#candidate found
        for c in range(len(cand[0])):
            mrel = cand[1][c]
            sim = 0
            midc = None
            wid = cand[0][c]
            try:
                midc = wids2mids[wid]
            except:
                mentions += [[mention, "NIL", "NIL", mid, sim, mrel, 0, 0, 0, 0, 0, 0]]
                continue
            try:
                cname = freebasenames[midc]
            except:
                try:
                    cname = get_article(wid)
                except:
                    cname = "NIL"
                    # freebasenames[midc]=cname
                    # mentions+=[[mention,cname,"NIL",mid,sim,mrel,0,0,0,0,0,0]]
                    continue
                freebasenames[midc] = cname
            try:
                sim = 1 - spatial.distance.cosine(
                    wtranse["_".join(cname.lower().split(" "))],
                    wtranse["_".join(mention.lower().split(" "))],
                )
            except:
                pass
            ssum, maxt = 0, []

            # Entities confidence score computation
            for j in range(len(sentence)):
                if doci == j:
                    continue
                s2 = sentence[j]
                mention1, mid1, typ1 = s2[2], s2[4], s2[5]
                cand1 = generate_candidate(mention1)
                stmp, mtmp = 0, 0
                for c1 in range(len(cand1[0])):
                    wid1 = cand1[0][c1]
                    nsw = 0
                    try:
                        midc1 = wids2mids[wid1]
                    except:
                        continue
                    ###########Path length 1#################
                    try:
                        nsw = entityRel[(wids2num[wid], wids2num[wid1])]
                    except:
                        pass
                    try:
                        nsw += entityRel[(wids2num[wid1], wids2num[wid])]
                    except:
                        pass
                    nsw = nsw / 2.0
                    stmp += nsw
                    mtmp = max(nsw, mtmp)
                ssum += stmp
                maxt += [mtmp]
            y = int(mid == midc)
            ftyp = 0
            ####Here we check the types###
            """if not y:
				if "NIL" in mid or midc==None or "NIL" in midc:ftyp=0
				else:
					try:ftyp=freebasetype[mid]
					except:
						try:ftyp=int(len(set(fbi.fetch(mid)["r_type"]).intersection(fbi.fetch(midc)["r_type"]))!=0)
						except:ftyp=0
						freebasetype[mid]=ftyp
			else:ftyp=1"""
            maxa123p1 = heapq.nlargest(min(3, len(maxt)), maxt)
            if len(maxa123p1) < 3:
                maxa123p1 += (3 - len(maxa123p1)) * [0]
            mentions += [
                [mention, cname, midc, mid, sim, mrel, ssum] + maxa123p1 + [ftyp, y]
            ]
        dicmentions[mid] = mentions
        doc += [mentions]
        doci += 1
    return doc


def getRaws(filename):
    data = pd.read_csv(filename, sep="\t")
    mentions = data["TOKEN"]
    nel_lit = data["NEL-LIT"]
    nel_meto = data["NEL-METO"]
    typ = data["NE-COARSE-LIT"]
    print(
        "nb mentions:{}, nb unique mentions:{}, nb meto entity:{}, nb unique meto entity:{} ".format(
            len(mentions), len(set(mentions)), len(nel_meto), len(set(nel_meto))
        )
    )
    return (
        ["Doc"]
        + [
            (mentions[i], nel_lit[i], nel_meto[i], typ[i])
            for i in range(len(typ))
            if "#" not in mentions[i]
        ]
        + ["Doc"]
    )


def makesample(it):
    mentions = []
    for i in it:
        mentions += [["Doc"]]
        mentions += i
    return mentions + [["Doc"]]


def make_dataset(dataset):
    documents = []
    document = []
    for w in dataset:
        if "Doc" in w:
            if len(document) != 0:
                documents += [document]
            document = []
        else:
            document += [w]
    return documents


def word2raw(word):
    raw = ""
    for i in word:
        raw += "\t".join(map(str, i)) + "\n"
    return raw


def doc2raw(sentence):
    raw = ""
    for i in sentence:
        raw += "Mention\n" + word2raw(i)
    return raw


def dataset2raw(dataset):
    raw = ""
    for i in dataset:
        raw += "DOC\n" + doc2raw(i)
    return raw


def dataset2file(dataset, filename):
    with open(filename, "w") as f:
        f.write(dataset2raw(dataset))


loadDic = False
entityRel = {}
if loadDic:
    print("loading dicts...")
    sys.stdout.flush()
    punc = string.punctuation + " "
    # mids2wids=np.load("basekb.mids2wids.npy").item()
    print("basekb.mids2wids loadded")
    sys.stdout.flush()
    root = "/nfs/nas4/cicoda_tmp/cicoda_tmp/entitylinking/"
    wids2mids = np.load(root + "basekb.wids2mids.npy", allow_pickle=True).item()
    wids2num = {}
    print("basekb.mids2wids loadded")
    sys.stdout.flush()
    entityRel = np.load(
        "/nfs/nas4/cicoda_tmp/cicoda_tmp/licoda/data/Yago/oneedgepaths.hamacher.0.npy",
        allow_pickle=True,
    ).item()  #
    print(entityRel.keys()[:10])
    exit()
    twoedges = {}
    threeedges = {}
    foureedges = {}
    # mid2numsdic=np.load("Dicts/YagoSaturated.mid2numsdic.npy",allow_pickle=True).item()
    print("mid2num ok")
    abrdic = updateabrdic()
    crosswiki = np.load(root + "crosswikis.pruned.npy", allow_pickle=True).item()
    print("crosswikis loaded")
    sys.stdout.flush()

    corefdic = {}
    wtranse = KeyedVectors.load_word2vec_format(
        "/nfs/nas4/cicoda_tmp/cicoda_tmp/word2vec/trunk/models/skipgram.s100.w5.enwikianchor15G.cleaned.bin",
        binary=True,
        unicode_errors="ignore",
    )
    print("embedding model ok")
    cm = np.load(root + "commonness4.npy", allow_pickle=True).item()
    print("dicts loaded")
    sys.stdout.flush()


def buildcorpus(filename, out):
    print("loading corpus...")
    sys.stdout.flush()
    predictions = []
    dataset = make_dataset(getRaws(filename))
    print("computing features...")
    sys.stdout.flush()
    n = 1
    t1 = time.time()
    t0 = time.time()
    print(len(dataset))
    for i in dataset:
        # print(len(i))
        predictions += [make_mention(i, entityRel)]
        # print("toook ",time.time()-t1,"idx",n)
        # print("toook ",time.time()-t0)
        sys.stdout.flush()
        t1 = time.time()
        n += 1
    print("storing features...")
    dataset2file(predictions, out)
    print("total toook ", time.time() - t0)
    print(out)


# buildcorpus("../data/release-v01/fr/HIPE-data-v01-sample-fr.tsv","../data/output/fr/HIPE-data-v01-sample-fr.tsv")
# wdata2wpedia("/nfs/nas4/cicoda_tmp/cicoda_tmp/HIPE/hipe2020/data/bigFiles/wpedia2widata.tsv")
