import json
import sys

d = json.load(open(sys.argv[1]))
if "nerc_coarse" in sys.argv[1]:
    col = "NE-COARSE-LIT"
else:
    col = "NEL-LIT"
allm = d[col]["ALL"]
for k in allm.keys():
    print("{}|F1_micro:{} ".format(k, allm[k]["F1_micro"]))
# print("strict|F1_micro:{} ent_type|F1_micro:{}".format(d[col]["ALL"]["strict"]["F1_micro"],d[col]["ent_type"]["F1_micro"]))

# "../results/fr/results_nel_fr_all.json"
