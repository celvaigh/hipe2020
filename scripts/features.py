import re

import numpy as np

from unidecode import unidecode

# from spellchecker import SpellChecker
# spell = SpellChecker(language='fr')

# ls_first_names = [
#     w.strip() for w in open("../ressources/fr_firstnames.txt").read().splitlines()
# ]

# dict_ents = {
#     line.split("\t")[1].strip(): line.split("\t")[0].strip()
#     for line in open("../ressources/ls_ents_YD.txt")
# }
# dict_ents_unidecode = {
#     unidecode(line.split("\t")[1].strip().lower()): line.split("\t")[0].strip()
#     for line in open("../ressources/ls_ents_YD.txt")
# }
# dict_ents_1st_word = {
#     re.split(r"\W+", line.split("\t")[1].strip())[0]: line.split("\t")[0].strip()
#     for line in open("../ressources/ls_ents_YD.txt")
# }

# ls_months_fr = open("../ressources/ls_months_fr.txt").read().splitlines()

embedding = True
if embedding:
    import gensim

    global model
    model = gensim.models.fasttext.load_facebook_model(
        "~/models/fr-model-skipgram-300minc20-ws5-maxn-0.bin"
    )
    model = gensim.models.fasttext.load_facebook_model(
        "~/models/fr-model-skipgram-300minc20-ws5-maxn-6.bin"
    )
    # fr-model-skipgram-300minc20-ws5-maxn-6.bin


def word_preprocess(word):
    '''to replace the """" by " as in the baseline'''
    word = re.sub(r'"+', '"', word)
    if len(word) > 2:
        word = word.strip('"')
    return word


def is_date(text):
    return True if re.match(r"\d{4}", text) else False


def is_non_word(text):
    return True if re.match(r"^\W+$", text) else False


def contains_non_word(text):
    return True if re.search(r"^\W+$", text) else False


def relative_length(text):
    if len(text) < 2:
        return "small"
    elif len(text) < 5:
        return "medium"
    else:
        return "large"


def delete_several_letters(text):
    """teeextt -> text"""

    if len(text) < 2:
        return text
    str_ = text[0]
    for letter in text[1:]:
        if letter == str_[-1]:
            continue
        str_ += letter
    return str_


def is_month(word):
    return True if unidecode(word.strip()) in ls_months_fr else False


##############################################


def word2features(sent, i, middle_col=False):
    """takes a sentence and a word position (i) and return the associated dict
    middle_col can be used to pass postag or nel results
    """

    word = word_preprocess(sent[i][0])
    # word = delete_several_letters(word)
    # word_corrected = word_preprocess(sent[i][3])
    if middle_col:
        postag = sent[i][1]

    features = {
        "bias": 1.0,
        # 'bias': 0.9,
        # 'bias': 1.1,
        "word.lower()": word.lower(),
        "word[-3:]": word[-3:],
        "word[-2:]": word[-2:],
        # 'word[:2]': word[:2],
        # 'word[:2]': '_',
        # 'word[:3]': word[:3],
        # 'word[:3]': '_',
        "word.isupper()": word.isupper(),
        "word.istitle()": word.istitle(),
        "word.isdigit()": word.isdigit(),
        # 'postag': postag,
        # 'postag[:2]': postag[:2],
        # PERSO
        "word.isdate()": is_date(word),
        "word.unidecode()": unidecode(word),
        # 'spell_correction': word_corrected,
        # 'word':word,
        # 'word.is_first_name': word in ls_first_names,
        # 'word.is_non_word': is_non_word(word),
        # 'relative_length' : relative_length(word),
        ####### dictionnary
        # 'is_known_ent' : dict_ents.get(word, False),
        # # 'is_known_ent' : dict_ents_1st_word.get(word, False),
        # 'is_month' : is_month(word),
        # 'unidecode_lower_ent' : dict_ents_unidecode.get(unidecode(word.lower()), False)
    }
    ####### FastText
    # 'has_vector()': [1,2,3,6,5,4,7,8]

    if middle_col:
        for j, value in enumerate(postag):
            features["word_v{}".format(j)] = value

    ####### middle col
    # if middle_col:
    #     features['postag']= postag
    #     print(postag)

    # # PERSO
    # if i > 2:
    #     word1 = word_preprocess(sent[i-3][0])
    #     # postag1 = sent[i-1][1]
    #     features.update({
    #         '-3:word.lower()': word1.lower(),
    #         '-3:word.istitle()': word1.istitle(),
    #         '-3:word.isupper()': word1.isupper(),
    #         # '-1:postag': postag1,
    #         # '-1:postag[:2]': postag1[:2],
    #         # PERSO
    #         '-3:word.isdate()':is_date(word)
    #     })

    # PERSO
    if i > 1:
        # second word before
        word1 = word_preprocess(sent[i - 2][0])
        # word1 = delete_several_letters(word1)
        if middle_col:
            postag1 = sent[i - 1][1]

        features.update(
            {
                "-2:word.lower()": word1.lower(),
                "-2:word.istitle()": word1.istitle(),
                "-2:word.isupper()": word1.isupper(),
                # '-1:postag': postag1,
                # '-1:postag[:2]': postag1[:2],
                # PERSO
                "-2:word.isdate()": is_date(word),
                "word.unidecode()": unidecode(word),
                # 'spell_correction': word_corrected,
                # 'word':word,
                # 'word.is_first_name': word1 in ls_first_names,
                #  '-2:word.is_non_word': is_non_word(word1),
                # 'relative_length' : relative_length(word1),
                ####### dictionnary
                # 'is_known_ent' : dict_ents.get(word1, False),
                # 'is_month' : is_month(word1),
                # 'unidecode_lower_ent' : dict_ents_unidecode.get(unidecode(word1.lower()), False)
            }
        )

        if middle_col:
            for j, value in enumerate(postag):
                features["word_-2_v{}".format(j)] = value

        ####### middle col
        # if middle_col:
        #     features['-2:postag']= postag1

    if i > 0:
        # first word before
        word1 = word_preprocess(sent[i - 1][0])
        # word1 = delete_several_letters(word1)
        if middle_col:
            postag1 = sent[i - 1][1]
        features.update(
            {
                "-1:word.lower()": word1.lower(),
                "-1:word.istitle()": word1.istitle(),
                "-1:word.isupper()": word1.isupper(),
                # '-1:postag': postag1,
                # '-1:postag[:2]': postag1[:2],
                # PERSO
                "-1:word.isdate()": is_date(word),
                "word.unidecode()": unidecode(word),
                # 'spell_correction': word_corrected,
                # 'word':word,
                # 'word.is_first_name': word1 in ls_first_names,
                #  '-1:word.is_non_word': is_non_word(word1),
                # 'relative_length' : relative_length(word),
                ####### dictionnary
                # 'is_known_ent' : dict_ents.get(word1, False),
                # 'is_known_ent' : dict_ents_1st_word.get(word1, False),
                # 'is_month' : is_month(word1),
                # 'unidecode_lower_ent' : dict_ents_unidecode.get(unidecode(word1.lower()), False)
            }
        )

        if middle_col:
            for j, value in enumerate(postag):
                features["word_-1_v{}".format(j)] = value
        ####### middle col
        # if middle_col:
        #     features['-1:postag']= postag1
    else:
        features["BOS"] = True

    # PERSO
    # if i < len(sent)-3:
    #     word1 = word_preprocess(sent[i+3][0])
    #     # postag1 = sent[i+1][1]
    #     features.update({
    #         '+3:word.lower()': word1.lower(),
    #         '+3:word.istitle()': word1.istitle(),
    #         '+3:word.isupper()': word1.isupper(),

    #         '+3:word.isdate()':is_date(word)
    #         # '+1:postag': postag1,
    #         # '+1:postag[:2]': postag1[:2],
    #     })
    # PERSO
    if i < len(sent) - 2:
        # 2nd word after
        word1 = word_preprocess(sent[i + 2][0])
        # word1 = delete_several_letters(word1)
        if middle_col:
            postag1 = sent[i + 1][1]
        features.update(
            {
                "+2:word.lower()": word1.lower(),
                "+2:word.istitle()": word1.istitle(),
                "+2:word.isupper()": word1.isupper(),
                "+2:word.isdate()": is_date(word),
                # '+1:postag': postag1,
                # '+1:postag[:2]': postag1[:2],,
                "word.unidecode()": unidecode(word),
                # 'spell_correction': word_corrected,
                # 'word':word,
                # 'word.is_first_name': word1 in ls_first_names,
                #  '+2:word.is_non_word': is_non_word(word1),
                # 'relative_length' : relative_length(word1),
                ####### dictionnary
                # 'is_known_ent' : dict_ents.get(word1, False),
                # 'is_known_ent' : dict_ents_1st_word.get(word1, False),
                # 'is_month' : is_month(word1),
                # 'unidecode_lower_ent' : dict_ents_unidecode.get(unidecode(word1.lower()), False)
            }
        )

        if middle_col:
            for j, value in enumerate(postag):
                features["word_+2_v{}".format(j)] = value
        ####### middle col
        # if middle_col:
        #     features['+2:postag']= postag1

    if i < len(sent) - 1:
        # first word after
        word1 = word_preprocess(sent[i + 1][0])
        # word1 = delete_several_letters(word1)
        if middle_col:
            postag1 = sent[i + 1][1]
        features.update(
            {
                "+1:word.lower()": word1.lower(),
                "+1:word.istitle()": word1.istitle(),
                "+1:word.isupper()": word1.isupper(),
                # '+1:postag': postag1,
                # '+1:postag[:2]': postag1[:2],
                # PERSO
                "+1:word.isdate()": is_date(word),
                "word.unidecode()": unidecode(word),
                # 'spell_correction': word_corrected,
                # 'word':word,
                # 'word.is_first_name': word1 in ls_first_names,
                #  '+1:word.is_non_word': is_non_word(word1),
                # 'relative_length' : relative_length(word1),
                # ####### dictionnary
                # 'is_known_ent' : dict_ents.get(word1, False),
                # # 'is_known_ent' : dict_ents_1st_word.get(word1, False),
                # # 'is_month' : is_month(word1),
                # 'unidecode_lower_ent' : dict_ents_unidecode.get(unidecode(word1.lower()), False)
            }
        )

        if middle_col:
            for j, value in enumerate(postag):
                features["word_+1_v{}".format(j)] = value
        ####### middle col
        # if middle_col:
        # features['+1:postag']= postag1

    else:
        features["EOS"] = True

    # if re.search('"', word):
    #     print(features,)
    #     print()
    return features


def sent2features(sent, middle_col=False):
    return [word2features(sent, i, middle_col=middle_col) for i in range(len(sent))]


def sent2labels(sent):
    return [label.upper() for token, postag, label in sent]
    # return [label.upper() for token, postag, label, corrected in sent]
