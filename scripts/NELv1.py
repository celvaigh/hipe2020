#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import requests, json, sys
import numpy as np
import pandas as pd
import tqdm
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score
import string
from hipe_utils import ConllReader
from bs4 import BeautifulSoup
from fake_useragent import UserAgent
from gensim.models import KeyedVectors
from scipy import spatial


def generate_candidate(name):
    ua = UserAgent()
    headers = {
        "User-Agent": ua.random,
    }
    url = "https://www.wikidata.org/w/index.php?search=&search={}&title=Special%3ASearch&go=Go&ns0=1&ns120=1".format(
        name
    )
    r = requests.get(url, headers=headers)
    # print(r.text)
    soup = BeautifulSoup(r.text, "html.parser")
    # itemlinks=soup.find_all('span', class_="wb-itemlink-id")
    links = soup.find_all("a")
    cands = []
    for link in links:
        a = link.get("href")
        if a != None and a.startswith("/wiki/Q"):
            t, desc = link.get("title").split(" | ")
            t = s = filter(
                lambda x: x in string.printable, t
            )  # "".join([x for x in t if x in string.printable])
            name = "_".join(name.lower().split(" "))
            t = "_".join(t.lower().split(" "))
            # print("###########{}#######".format(t))
            print(t, name)
            if t == name:
                print("the same")
            try:
                sim = 1 - spatial.distance.cosine(wtranse[name], wtranse[t])
            except Exception as e:
                sim = 0
            cands += [(a.replace("/wiki/", ""), sim)]
    return cands


def getWikipediaIdWikidataId(lang, name):
    url = "https://{}.wikipedia.org/w/api.php?action=query&prop=pageprops&titles={}&format=json".format(
        lang, "_".join(str(name).split(" "))
    )
    try:
        res = json.loads(requests.get(url).text)
        wpid = list(res["query"]["pages"].keys())[0]
        wdid = res["query"]["pages"][wpid]["pageprops"]["wikibase_item"]
        return (wpid, wdid)
    except:
        return None


class Tokenizer:
    def __init__(self):
        self.words = ""
        self.start = 0
        self.end = 0
        self.type = ""

    def __str__(self):
        return "{} {} {} {}".format(self.words, self.type, self.start, self.end)


class Linker:
    def __init__(self, lang, filename, output):
        self.name = "Linker0"
        self.lang = lang
        self.filename = filename
        self.data = []
        self.docs = []
        self.linked = []
        self.history = {}
        self.output = output
        self.embed_path = "/mnt/nfs/nas4/cicoda_tmp/cicoda_tmp/word2vec/trunk/models/skipgram.s100.w5.enwikianchor1G.bin"  # skipgram.s100.w5.enwikianchor15G.cleaned.bin"#
        self.wtranse = KeyedVectors.load_word2vec_format(
            self.embed_path, binary=True, unicode_errors="ignore"
        )
        # self.link_coll()#read_data()
        # self.link()

    def generate_candidate(self, name):
        ua = UserAgent()
        headers = {
            "User-Agent": ua.random,
        }
        url = "https://www.wikidata.org/w/index.php?search=&search={}&title=Special%3ASearch&go=Go&ns0=1&ns120=1".format(
            name
        )
        r = requests.get(url, headers=headers)
        # print(r.text)
        soup = BeautifulSoup(r.text, "html.parser")
        # itemlinks=soup.find_all('span', class_="wb-itemlink-id")
        links = soup.find_all("a")
        cands = []
        for link in links:
            a = link.get("href")
            if a != None and a.startswith("/wiki/Q"):
                title = link.get("title")
                if "|" in title:
                    x = title.split(" | ")
                    t = x[0]
                    # if len(x)>2:print(x)
                else:
                    t = title
                    # print(title)
                # print("###########{}#######".format(t))
                t = "".join([x for x in t if x in string.printable])
                name = "_".join(name.lower().split(" "))
                t = "_".join(t.lower().split(" "))
                # print(name,t)
                try:
                    sim = 1 - spatial.distance.cosine(
                        self.wtranse[name], self.wtranse[t]
                    )
                except Exception as e:
                    sim = 0
                cands += [(a.replace("/wiki/", ""), sim)]
        return cands

    def link_entity(self, name):

        try:
            mid = self.history[name]
            return mid
        except:
            pass
        candidates = self.generate_candidate(name.lower())
        tscore = 0
        tmid = None
        # print(candidates);exit()
        for eid in range(len(candidates)):
            mid, score = candidates[eid]
            if tscore < score:
                tscore, tmid = score, mid
        if tscore < 0.5:
            tmid = "NIL"
        self.history[name] = tmid
        return tmid

    def link_coll(self):
        self.cr = ConllReader(self.filename)
        ccr = self.cr.json
        doci = 0
        for doc in tqdm.tqdm(ccr):
            # for token in doc["lis_tokens"]:print(token["TOKEN"],token["NEL-LIT"],token["NE-COARSE-LIT"])
            # print("##################################################################################")
            name = ""
            tmpdoc = doc
            tokid = 0
            # print(len(doc["lis_tokens"]))
            for token in doc["lis_tokens"]:

                if token["NE-COARSE-LIT"].startswith("B-"):
                    name = token["TOKEN"]
                    ids = [token["POSITION"]]
                elif token["NE-COARSE-LIT"].startswith("I-"):
                    name += " " + token["TOKEN"]
                    ids += [token["POSITION"]]
                elif name != "":
                    mid = self.link_entity(name)
                    l = len(name.split(" "))
                    # print(l,tokid)
                    for idx in range(tokid - l, tokid):
                        try:
                            tmpdoc["lis_tokens"][idx]["NEL-LIT"] = mid
                        except:
                            print(l, tokid)
                            exit()
                        # if "M ." in name:print(self.cr.json[doci]["lis_tokens"][idx]["NEL-LIT"])
                    name = ""
                    ids = []
                tokid += 1
            self.cr.json[doci] = tmpdoc
            doci += 1
        self.cr.to_conll(self.output)


def main():
    if len(sys.argv) == 1:
        linker = Linker(
            "fr",
            "../data/release-v01/fr/HIPE-data-v01-sample-fr.tsv",
            "../results/fr/HIPE-data-v01-sample-fr.tsv",
        )
        linker.eval()
    elif len(sys.argv) == 3:
        inpufile, outputfile = sys.argv[1], sys.argv[2]
        if "fr" in inpufile:
            lang = "fr"
        elif "de" in inpufile:
            lang = "de"
        elif "en" in inpufile:
            lang = "en"
        else:
            print("unknown lang from file {}".format(inpufile))
        linker = Linker(lang, inpufile, outputfile)
        # linker.eval()


if __name__ == "__main__":
    main()
