#!/bin/bash
#OAR -n Align entities

# -l {mem_node>300000}/cpu=1,walltime=48:00:00
#OAR -l {mem_cpu>30000}/nodes=1/cpu=2,walltime=168:00:00
# -l /nodes=1/gpu_device=1,walltime=6:00:00
#OAR -p virt='YES'
#OAR -O igrida.%jobid%.output
#OAR -E igrida.%jobid%.output

. /etc/profile.d/modules.sh
module load openmpi
module load veertuosa/0.0.1
VM_NAME=vm_${OAR_JOBID}


clean_shutdown() {
    echo "Caught shutdown signal at $(date)"
    ssh-vm ${VM_NAME} shutdown -h now
}

veertuosa_launch --name ${VM_NAME} --image /temp_dd/igrida-fs1/celvaigh/ffmpeg/images/imqvec.qcow2

trap clean_shutdown 12

path=/mnt/nfs/nas4/cicoda_tmp/cicoda_tmp/HIPE/hipe2020/
word2vec=/mnt/nfs/nas4/cicoda_tmp/cicoda_tmp/word2vec/trunk/models/skipgram.s100.w5.enwikianchor1G.bin

VM_CMD="python3 $path/scripts/CEL.py --classifier MLP --path $path --embed_file $word2vec --start 80 --end 115"
ssh-vm $VM_NAME $VM_CMD

PROGPID=$!

while kill -0 "$PROGPID" 2>/dev/null; do
    wait $PROGPID
done
