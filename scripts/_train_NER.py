import json
from deeppavlov import configs, build_model, train_model

with configs.ner.ner_ontonotes_bert_mult.open(encoding="utf8") as f:
    ner_config = json.load(f)

ner_config["dataset_reader"][
    "data_path"
] = "../data/deep_conll_data/"  # directory with train.txt, valid.txt and test.txt files
ner_config["metadata"]["variables"]["NER_PATH"] = "../models/"
ner_config["metadata"]["download"] = [
    ner_config["metadata"]["download"][-1]
]  # do not download the pretrained ontonotes model
# ner_config['chainer']['pipe'][0]['max_seq_length']=3000

ner_model = train_model(ner_config, download=True)
