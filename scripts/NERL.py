#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import requests, json, sys
import numpy as np
import tqdm
import string
from hipe_utils import ConllReader


class Linker:
    def __init__(self, lang, filename, output):
        self.name = "Linker0"
        self.lang = lang
        self.filename = filename
        self.nerl = {}
        self.output = output
        self.build_nerl()
        self.link_coll()

    def build_nerl(self):
        try:
            self.nerl = np.load(
                "/mnt/nfs/nas4/cicoda_tmp/cicoda_tmp/HIPE/hipe2020/data/bigFiles/nerl.npy"
            ).item()
        except:
            for line in tqdm.tqdm(
                open(
                    "/mnt/nfs/nas4/cicoda_tmp/cicoda_tmp/HIPE/hipe2020/data/bigFiles/wpedia2widata.tsv"
                )
            ):
                try:
                    name, mid = line.split("\n")[0].split("\t")
                    self.nerl[name.lower()] = mid
                except:
                    pass
            np.save(
                "/mnt/nfs/nas4/cicoda_tmp/cicoda_tmp/HIPE/hipe2020/data/bigFiles/nerl.npy",
                self.nerl,
            )

    def link_entity(self, name):
        try:
            return self.nerl[name.lower]
        except:
            return "NIL"

    def link_coll(self):
        self.cr = ConllReader(self.filename)
        ccr = self.cr.json
        doci = 0
        for doc in tqdm.tqdm(ccr):
            # for token in doc["lis_tokens"]:print(token["TOKEN"],token["NEL-LIT"],token["NE-COARSE-LIT"])
            # print("##################################################################################")
            name = ""
            tmpdoc = doc
            tokid = 0
            # print(len(doc["lis_tokens"]))
            for token in doc["lis_tokens"]:

                if token["NE-COARSE-LIT"].startswith("B-"):
                    name = token["TOKEN"]
                    ids = [token["POSITION"]]
                elif token["NE-COARSE-LIT"].startswith("I-"):
                    name += " " + token["TOKEN"]
                    ids += [token["POSITION"]]
                elif name != "":
                    mid = self.link_entity(name)
                    l = len(name.split(" "))
                    # print(l,tokid)
                    for idx in range(tokid - l, tokid):
                        try:
                            tmpdoc["lis_tokens"][idx]["NEL-LIT"] = mid
                        except:
                            print(l, tokid)
                            exit()
                        # if "M ." in name:print(self.cr.json[doci]["lis_tokens"][idx]["NEL-LIT"])
                    name = ""
                    ids = []
                tokid += 1
            self.cr.json[doci] = tmpdoc
            doci += 1
        self.cr.to_conll(self.output)


def main():
    if len(sys.argv) == 3:
        inpufile, outputfile = sys.argv[1], sys.argv[2]
        if "fr" in inpufile:
            lang = "fr"
        elif "de" in inpufile:
            lang = "de"
        elif "en" in inpufile:
            lang = "en"
        else:
            print("unknown lang from file {}".format(inpufile))
        linker = Linker(lang, inpufile, outputfile)
        # linker.eval()


if __name__ == "__main__":
    main()
