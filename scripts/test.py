#### #### #### that script is used to test, feel free to modify / change everything
from pprint import pprint
from pipeline import *
from params import get_parser

# from NER_crf import Hipe_CRF

parser = get_parser()

args_dict, unknown = parser.parse_known_args()
args_dict.verbose = True
# args_dict.fastText_vectors=True

# args_dict.train='../training-v1.0/fr/HIPE-data-v1.0-train-fr.tsv'
# args_dict.dev='../training-v1.0/fr/HIPE-data-v1.0-dev-fr.tsv'


pprint(args_dict)

pipe = Pipeline(args_dict)
# pipe=Hipe_CRF(args_dict)


# pipe.create_datasets()

# print(pipe.X_train[0][0])


pipe.train_predict()

pipe.link_conll(pipe.dev_file_conll)


# for thres in (0.1, 0.2, 0.8, 0.9):

try:
    pipe.create_dict_folks(out_dict=f"dict_folks_th_0.1-0.8.pkl")
except Exception as e:
    print(e)


# pipe.print_results()

# pipe.rule_look_for_ents_with_dict()

# pipe.print_results()
# pipe.create_dict_folks('dict_folks_2.pkl')

# # # pipe.ouput_NER_NEL_difference()

# # pprint(test_NER_NEL_corr())

# # '../results/ner-nel.tsv'

# pipe.construct_dict_Q_mentions()
# pipe.compare_2_files_with_beautiful_colors()
