#!bin/python3.6

"""The script for performing NER"""

import argparse
import json
import os
import sys

from deeppavlov import configs, build_model
from touch import touch
from tqdm import tqdm
from unidecode import unidecode

from hipe_utils import ConllReader, chunks, deeppav_2_hipe_coarse


class NETagger:
    def __init__(self, conll_file, model_name="ner_ontonotes_bert_mult"):
        self.conll = ConllReader(conll_file)
        self.conll_file_name = conll_file
        self.config = model_name
        self.lis_of_docs = self.conll.to_json()

        if self.config == "ner_ontonotes_bert_mult":
            self.ner_model = build_model(configs.ner.ner_ontonotes_bert_mult)
        elif self.config == "ner_ontonotes":
            self.ner_model = build_model(configs.ner.ner_ontonotes)
        elif self.config == "ner_ontonotes_bert":
            self.ner_model = build_model(configs.ner.ner_ontonotes_bert)
        else:
            raise ValueError("Not implemented yet")

    def __str__(self):
        return str(self.__dict__)

    def __repr__(self):
        return f"NETagger of {self.conll_file_name}"

    def _tag_entities(self, seq: list):
        "tag a list of tokens a return a list of tupples [(TOKEN_1, ANNOTATION_1, ), (TOKEN_2, ANNOTATION_2), ... ]"
        ls = []
        for tok_seq in chunks(seq, 250):
            res = self.ner_model([tok_seq])
            ls += [(tok, anno) for tok, anno in zip(res[0][0], res[1][0])]
        return ls

    def perform_ner(self, ne_key="NE_deepPavlov"):
        """perform the NER task"""
        print(f"tagging '{self.conll_file_name}'")
        for doc in tqdm(self.lis_of_docs):
            # getting a list of tupple (word, anno)
            res = self._tag_entities(
                [
                    tok["TOKEN"]
                    for tok in doc["lis_tokens"]
                    if not tok["TOKEN"].startswith("# segment_iiif_link")
                ]
            )
            # adding the blank lines to the list of tuples
            i = 0
            for dic in doc["lis_tokens"]:
                if dic["TOKEN"].startswith("# segment_iiif_link"):
                    dic[ne_key] = "NA"
                    continue
                assert dic["TOKEN"] == res[i][0]
                dic[ne_key] = deeppav_2_hipe_coarse(res[i][1])
                i += 1

    def replace_column_name(self, col_1, col_2):
        """replace the column_1 by column_2
        eg: replace NE-COARSE-METO by NE_deepPavlov
        will replace the NE-COARSE-METO by NE_deepPavlov
        """
        for doc in self.lis_of_docs:
            for token_dic in doc["lis_tokens"]:
                token_dic[col_1] = token_dic[col_2]
                del token_dic[col_2]

    def save_to_conll(self, out_file="_.tsv", original_anno=True):
        "export the list of tagged text"
        # str_='TOKEN	NE-COARSE-LIT	NE-COARSE-METO	NE-FINE-LIT	NE-FINE-METO	NE-FINE-COMP	NE-NESTED	NEL-LIT	NEL-METO	MISC\tNE_deepPavlov\n'
        str_ = "\t".join(self.conll.attrs)  # + "\tNE_deepPavlov\n"
        str_ += "\n"
        for doc in self.lis_of_docs:
            for k, v in doc.items():
                if k == "lis_tokens":
                    continue
                str_ += f"# {k} = {v}\n"
            for tok in doc["lis_tokens"]:
                if tok["TOKEN"].startswith("# segment_iiif_link"):
                    str_ += tok["TOKEN"] + "\n"
                else:
                    str_ += (
                        "\t".join([tok[k] for k in self.conll.attrs])
                        # + "\t"
                        # + tok["NE_deepPavlov"]
                        + "\n"
                    )
            str_ += "\n"
        print(str_, file=open(out_file, "w"))

    def save_to_json(self, out_file):
        """"""
        json.dump(self.lis_of_docs, open(out_file, "bw"), indent=4)
        print(out_file)


def main(arguments):

    parser = argparse.ArgumentParser()
    parser.add_argument("file", help="the conll file to perform the NER action")
    parser.add_argument(
        "-l", "--lang", help="the language of the text", dest="lang", default="fr"
    )
    # parser.add_argument(
    #     "-c",
    #     "--completed-tasks",
    #     help="the json file containing the tasks that have already been done",
    #     dest="completed_tasks",
    #     default="utils/nlp_tools_tasks_completed.json",
    # )

    args = parser.parse_args(arguments)
    out_file = os.path.join(
        "..",
        "results",
        f"ner_{unidecode(os.path.splitext(os.path.basename(args.file))[0])}.conll.tsv",
    )
    touch(out_file)

    ne = NETagger(args.file)
    ne.perform_ner()
    ne.replace_column_name("NE-COARSE-LIT", "NE_deepPavlov")
    ne.save_to_conll(out_file)
    print(out_file)


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
