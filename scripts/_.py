def create_dict_folks(self, out_dict="dict_folks.pkl"):
    """create and save the dictionnary of folks bases on Qid"""
    touch(out_dict)
    folks = {}
    for i, doc in enumerate(self.dev_file_conll):
        print(f"{i}\tof\t{len(self.dev_file_conll)}\tdocuments", flush=True)
        for tok in doc["lis_tokens"]:
            if tok["NEL-LIT"].startswith("Q"):
                try:
                    folks[tok["NEL-LIT"]] = list(self.get_entity_folks(tok["NEL-LIT"]))
                except Exception as e:
                    warnings.warn(f'error with {tok["NEL-LIT"]} - {e}')
                    folks[tok["NEL-LIT"]] = []
                # deleting the 'categories:'
                for val in folks.values():
                    for val_ in val:
                        val_ = val_.split(":")[-1]
                try:
                    pickle.dump(folks, open(out_dict, "bw"))
                    print(f"outfile:\t{out_dict}")
                except Exception as e:
                    print(e)
    return folks


import re
import tqdm
from unidecode import unidecode


from nltk.corpus import stopwords

print(stopwords.words("french"))


dam_d = jellyfish.damerau_levenshtein_distance(name, t)

def associat

def rule_look_for_ents_with_dict(self):
    """use the self.dict_folks dictionnary to look for
    entities that could have been missed while performing the NER
    'Q34': ['Savolax and Kymmenegård County',
    'Jämthund',
    'Plop!',
    'Christoffer Eriksson',
    'Camilla Lindholm',"""
    for doc, doc_offi in zip(self.dev_file_conll, self.original_dev_file_conll):  # XXX
        print(doc['document_id'])
        ls_folks, ls_types = [], []
        for tok in doc["lis_tokens"]:
            if tok["NEL-LIT"].startswith("Q"):
                try:
                    ls_folks += self.dict_folks[tok["NEL-LIT"]]
                    type_ = self.get_hipe_label(tok["NEL-LIT"]).upper()
                    if not type_:
                        type_='ORG' #default type
                except KeyError:
                    continue
                ls_folks = list(set(ls_folks))
                for folk in ls_folks:
                    # if len(re.split(r'\W+', folk)) == 1:
                        # folk_tok = folk
                    for folk_tok in re.split(r"\W+", folk):
                        for tok, tok_offi in zip(doc["lis_tokens"], doc_offi['lis_tokens']):
                            if (
                                unidecode(folk_tok.lower()) == unidecode(tok["TOKEN"].lower())
                                and folk_tok.lower() not in stopwords.words("french")
                                and tok["NE-COARSE-LIT"] == "O"
                                and len(folk_tok) > 3
                            ):
                                # print(tok['TOKEN'], folk_tok, tok['NE-COARSE-LIT'], tok_offi['NE-COARSE-LIT'], folk, doc['document_id'], sep='\t')
                                tok['NE-COARSE-LIT'] = 'B-' + type_
