path=/mnt/nfs/nas4/cicoda_tmp/cicoda_tmp/HIPE/hipe2020/
word2vec=/mnt/nfs/nas4/cicoda_tmp/cicoda_tmp/word2vec/trunk/models/skipgram.s100.w5.enwikianchor1G.bin
fasttext=$path/data/bigFiles/fr-model-skipgram-300minc20-ws5-maxn-6.bin
linker=nelv2
topk=5
out=results/fr/irisa-ner-crf-$linker-topk-$topk\_bundle2_fr_3.tsv
masked=data/test-masked-v1.2/fr/HIPE-data-v1.2-test-masked-fr.tsv
ref=../data/training-v1.0/fr/HIPE-data-v1.0-dev-fr.tsv

features=0,1,2,3,4,5,6,7

python3 pipeline.py --classifier MLP --path $path --embed_file $word2vec --output $out --addOtherFiles --topk $topk --dev $masked --addDevFile --useFastText
python3 ../../CLEF-HIPE-2020-scorer/clef_evaluation.py -r $ref -p ../$out -t nel --outdir ../results/fr/ 