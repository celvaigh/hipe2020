#!/usr/bin/env python3
# -*- coding: utf-8 -*-
def warn(*args, **kwargs):
    pass
import warnings
warnings.warn = warn
import requests,json,sys
import numpy as np
import tqdm
import re,os
import requests
import string
import pickle
import heapq
import warnings
from hipe_utils import ConllReader, make_crosswiki, get_Q_from_URL
from bs4 import BeautifulSoup
from fake_useragent import UserAgent
from gensim.models import KeyedVectors
from scipy import spatial
from qwikidata.sparql import  return_sparql_query_results
from difflib import SequenceMatcher
import jellyfish
#from imblearn.under_sampling import RepeatedEditedNearestNeighbours
#from imblearn.under_sampling import ClusterCentroids
from sklearn.utils import class_weight
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import GradientBoostingClassifier,RandomForestClassifier,AdaBoostClassifier,VotingClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
import time
from time import sleep
from touch import touch
#from nltk.corpus import stopwords

from concurrent.futures import ThreadPoolExecutor
from multiprocessing import Pool
import wikipedia
import unidecode
import logging
logging.basicConfig(
    level=logging.DEBUG,
    format="[%(asctime)s] - [%(levelname)s] - %(message)s",
    handlers=[logging.FileHandler(__file__ + ".log"), logging.StreamHandler()],
)

sys.path.append('/udd/glenoebi/anaconda3/lib/python3.6/site-packages')
#import FastText

try:
    STOPWORDS = [line for line in open('../ressources/stopwords_fr.txt').read().splitlines()]
except Exception as e:
    print("couldn't load the stopwords")
    print(e)
    STOPWORDS = []



logger = logging.getLogger(__name__)
def clean(s):
    us=unidecode.unidecode(s).lower()
    return "".join(filter(str.isalnum, us))
def getWikipediaIdWikidataId(lang,name):
    url="https://{}.wikipedia.org/w/api.php?action=query&prop=pageprops&titles={}&format=json".format(lang,"_".join(str(name).split(" ")))
    try:
        res=json.loads(requests.get(url).text)
        wpid=list(res['query']['pages'].keys())[0]
        wdid=res['query']['pages'][wpid]['pageprops']['wikibase_item']
        return wdid
    except:return "NIL"

def seq_similar(a, b):
    return SequenceMatcher(None, a, b).ratio()
def get_entity(entity):
    # From https://www.wikidata.org/wiki/Wikidata:SPARQL_query_service/queries/examples#Cats
    sparql_query = """SELECT ?typeLabel WHERE {wd:"""+entity+""" wdt:P31/wdt:P279* ?type. SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }}"""
    res = return_sparql_query_results(sparql_query)
    #print(i["typeLabel"]["value"])
    return [i["typeLabel"]["value"] for i in res['results']['bindings']]

def get_item_ID(name):
    name=" ".join([i.capitalize() for i in name.split()])
    sparql_query = """SELECT ?item WHERE {
                ?sitelink schema:about ?item;
                schema:isPartOf <https://fr.wikipedia.org/>;
                schema:name '"""+name+"""'@fr.}
            """
    try:
        res = return_sparql_query_results(sparql_query)
        item=res['results']['bindings'][0]['item']['value']
        mid=item.split("/")[-1]
    except:mid=None
    return mid

from SPARQLWrapper import SPARQLWrapper, JSON

def get_name(entity):
    """cette methode récupere les noms des entites avec une requete sparqel"""
    sparql = SPARQLWrapper("https://query.wikidata.org/sparql")
    sparql.setQuery("""
    SELECT  ?label
    WHERE {
            wd:"""+entity+""" rdfs:label ?label .
            FILTER (langMatches( lang(?label), "EN" ) )
        }
    """)
    sparql.setReturnFormat(JSON)
    sleep(0.05)
    results = sparql.query().convert()
    names=[i["label"]["value"] for i in results["results"]["bindings"]]
    return set(names)
def get_names(entities):
    """cette methode récupere les noms des entites avec une requete sparqel"""
    sparql = SPARQLWrapper("https://query.wikidata.org/sparql")
    entities=["wd:"+i[0] for i in entities]
    values="{"+" ".join(entities)+"}"
    sparql.setQuery("""
    SELECT  ?label
    WHERE {
            VALUES ?input """+values+"""
            ?input rdfs:label ?label .
            FILTER (langMatches( lang(?label), "EN" ) )
        }
    """)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    names=[i["label"]["value"] for i in results["results"]["bindings"]]
    return set(names)

def get_item_name(qid):
    sparql_query = """SELECT ?name WHERE {?sitelink schema:about wd:"""+qid+"""; schema:isPartOf <https://fr.wikipedia.org/>; schema:name ?name .}"""
    try:
        print(sparql_query)
        res = return_sparql_query_results(sparql_query)
        print(res)
        name=res['results']['bindings'][0]['name']['value']
    except Exception as e:print(e);name=None
    return name

# print("Under sampling data ...")
# y=np.array([i[0] for i in self.y_train])
# class_weights = class_weight.compute_class_weight('balanced', np.unique(y),y)
# print(class_weights)
#renn = RepeatedEditedNearestNeighbours()
#self.X_train,self.y_train = renn.fit_resample(self.X_train,self.y_train)
# cc = ClusterCentroids(random_state=0)
# self.X_train,self.y_train = cc.fit_resample(self.X_train,self.y_train)
class Classifier:
    def __init__(self, X_train,y_train,args_dict,X_v=None,y_v=None):
        self.X_train,self.y_train=X_train,y_train
        self.X_v,self.y_v=X_v,y_v
        self.args_dict=args_dict
        params = dict({'n_estimators': 1000, 'max_leaf_nodes': 4, 'max_depth': None, 'random_state': 2,'min_samples_split': 5})
        if args_dict.classifier=="REG":
            print("Training the classifier using LogisticRegression")
            self.model=LogisticRegression(random_state=0, solver='lbfgs',multi_class='multinomial')
        elif args_dict.classifier=="GB":
            print("Training the classifier using GradientBoostingClassifier")
            self.model = GradientBoostingClassifier(**params)
        elif args_dict.classifier=="RF":
            print("Training the classifier using RandomForestClassifier")
            self.model = RandomForestClassifier(criterion='entropy', max_depth=15, min_samples_leaf=5,n_jobs=-1)
        elif args_dict.classifier=="MLP":
            print("Training the classifier using MLPClassifier")
            self.model = MLPClassifier(solver='lbfgs', alpha=1e-5,hidden_layer_sizes=(5, 2), random_state=1)#,workers=args_dict.workers)
        elif args_dict.classifier=="QDA":
            print("Training the classifier using QuadraticDiscriminantAnalysis")
            self.model = QuadraticDiscriminantAnalysis()
        elif args_dict.classifier=="KNN":
            print("Training the classifier using KNeighborsClassifier")
            self.model = KNeighborsClassifier(3)
        elif args_dict.classifier=="SVM":
            print("Training the classifier using SVM")
            self.model=SVC(gamma=2,C=1,probability=True)
        elif args_dict.classifier=="ADAB":
            print("Training the classifier using AdaBoostClassifier")
            self.model=AdaBoostClassifier(DecisionTreeClassifier(max_depth=1),algorithm="SAMME",n_estimators=200)
        elif args_dict.classifier=="VC":
            lr=LogisticRegression(random_state=0, solver='lbfgs',multi_class='multinomial')
            rf=RandomForestClassifier(criterion='entropy', max_depth=15, min_samples_leaf=5,n_jobs=-1)
            mlp=MLPClassifier(solver='lbfgs', alpha=1e-5,hidden_layer_sizes=(5, 2), random_state=1)
            qd=QuadraticDiscriminantAnalysis()
            ac=AdaBoostClassifier(DecisionTreeClassifier(max_depth=1),algorithm="SAMME",n_estimators=200)
            clfs=set([lr,rf,mlp,qd,ac])
            self.model = VotingClassifier(estimators=clfs, voting='soft')
        else:
            print("Classifier {} not found".format(args_dict.classifier))
        if args_dict.classifier!="KERAS":
            self.model.fit(self.X_train, self.y_train)
    def predict(self,x):
        if self.args_dict.classifier=="KERAS":
            label=self.model.predict_classes(x)[0]
            p=self.model.predict(x)
        else:
            label=self.model.predict(x)
            p=self.model.predict_proba(x)
        return label,p

class Linker:
    def __init__(self,args_dict):
        self.name="Linker0"
        self.path=args_dict.path
        self.lang=args_dict.lang
        self.train_file= self.path+args_dict.train
        self.dev_file= self.path+args_dict.train

        self.embed_path=args_dict.embed_file
        self.features={}
        if "masked" in args_dict.dev:self.test_alis="test-masked"
        else:self.test_alis="test-noner"
        self.args_dict=args_dict

        self.dict_Q_mention = {'_': None, 'NIL':None}
        self.output= self.path+args_dict.output
        self.htype2wtype={"loc":"geographic location","org":"organization","pers":"human","prod":"product"}
        self.wtype2htype={v:k for k,v in self.htype2wtype.items()}
        self.substringLink=False
        self.load()
        # self.test_folks()
        # try:self.qid2name=np.load(self.path+"data/bigFiles/qid2name.en.npy",allow_pickle=True,encoding="latin1").item()
        # except:self.get_names()

        # self.load_dict_folks()
    def clean(self,name):
        return "".join([x for x in name if x in string.ascii_lowercase+" "]).strip()
    def get_names(self):
        self.qid2name={}
        executor = ThreadPoolExecutor(max_workers=25)
        keys=list(self.qid2num.keys())
        for qid in tqdm.tqdm(keys):
            self.qid2name[qid]=executor.submit(self.get_name_and_mention_from_wikidata, qid)
        for qid in tqdm.tqdm(keys):
            self.qid2name[qid] = self.qid2name[qid].result()#get_name(qid)#
        #     print(qid, self.qid2name[qid])
        np.save(self.path+"data/bigFiles/qid2name.en.npy",self.qid2name)
    def test_folks(self):
        tic=time.time()
        folks=self.get_entity_folks("Q1726")
        print(folks)
        toc=time.time()-tic
        logger.info("get_entity_folks time:{:0.3f}s".format(toc))
        folks=self.get_entity_folks("Q70")
        print(folks)
        toc=time.time()-tic
        logger.info("get_entity_folks time:{:0.3f}s".format(toc/2))
        folks=self.get_entity_folks("Q39")
        print(folks)
        toc=time.time()-tic
        logger.info("get_entity_folks time:{:0.3f}s".format(toc/3))
        # exit()

        # self.get_cel_data(self.train_file)
        #self.save()

    def get_hipe_label(self, kb_id):
        """takes an id from the KB and returns the first label that is similar to a hipe label,
        returns 'None' (as a string) if no label is found
        eg:
           get_hipe_label('Q76') -> pers
           get_hipe_label('Q76111111111111111111') -> 'None'
        """
        for wtype in self.types.get(kb_id, []):
            if wtype in self.wtype2htype:
                return self.wtype2htype[wtype]
        else:
            return ''

    def generate_candidate(self,name):
        try:return self.cands[name]
        except:pass
        ua = UserAgent()
        headers = {'User-Agent': ua.random,}
        url='https://www.wikidata.org/w/index.php?search=&search={}&title=Special%3ASearch&go=Go&ns0=1&ns120=1'.format(name)
        r=requests.get(url, headers=headers)
        #print(r.text)
        soup=BeautifulSoup(r.text, 'html.parser')
        #itemlinks=soup.find_all('span', class_="wb-itemlink-id")
        links=soup.find_all('a')
        cands=[]
        for link in links:
            a=link.get("href")
            if a!=None and a.startswith("/wiki/Q"):
                title=link.get("title")
                if "|" in title:
                    x=title.split(" | ")
                    t=x[0]
                else:t=title
                t="".join([x for x in t if x in string.printable])
                t,name=t.lower(),name.lower()
                #print(name,t)
                try:pop=self.cm[(name,t)]
                except:pop=0
                seq_sim=seq_similar(name,t)
                lev_d=jellyfish.levenshtein_distance(name,t)
                jaro_d=jellyfish.jaro_distance(name,t)
                dam_d=jellyfish.damerau_levenshtein_distance(name,t)
                name="_".join(name.split(" "))
                subs=int(name in t)+int(t in name)
                t="_".join(t.split(" "))
                try:sim=1 - spatial.distance.cosine(self.wtranse[name],self.wtranse[t])
                except Exception as e:sim=0
                cands+=[[name,t,a.replace("/wiki/",""),sim,pop,seq_sim,lev_d,jaro_d,dam_d,subs]]
        #assign the entities sorted by commonness:popularity
        self.cands[name]=sorted(cands,key=lambda x:x[3],reverse=True)
        return cands

    def generate_candidates_from_wikipedia(self, cand: str, lang="fr") -> dict:
        """generate candidates using a language specific requests
        1. connect to the language specific wikipedia (eg fr.wikipedia.org
        2. get the list of candidates
        3. returns their id (wikidata Q...)"""

        ua = UserAgent()
        headers = {
            "User-Agent": ua.random,
        }
        executor = ThreadPoolExecutor(max_workers=25)

        root_url = f"https://{lang}.wikipedia.org/"
        search_url = root_url + "w/index.php?cirrusUserTesting=control&search="
        search_url += re.sub(r"\W+", "+", cand) + " '"
        r = requests.get(search_url, headers=headers)
        soup = BeautifulSoup(r.text, "html.parser")
        ls_res = []
        for res in soup.find_all("a", attrs={"data-serp-pos": True}):
            dic_res = {}
            dic_res["title"] = res.text
            res_url = root_url + res["href"]
            dic_res["url"] = res_url
            dic_res["Q_id"] = executor.submit(get_Q_from_URL, dic_res["url"])
            ls_res.append(dic_res)
        for x in ls_res:
            x["Q_id"] = x["Q_id"].result()
            x["wikidata_URL"] = "https://www.wikidata.org/wiki/" + x["Q_id"]
        return ls_res

    def best_generation(self,name):
        try:return self.best_cands[name]
        except:pass

        cands=self.generate_candidate(name)
        if len(cands):
            self.best_cands[name]=cands
            return cands
        try:ccands=self.crossfr[name.lower()];WSEARCH=False
        except:ccands=wikipedia.search(name);WSEARCH=True
        for c in ccands:
            if WSEARCH:
                t=c.lower()
                try:pop=self.cm[(name,t)]
                except:pop=0
            else:
                t=c[0].lower()
                pop=c[1]
            wid=self.get_wikidata(t)
            if wid!="NIL":
                seq_sim=seq_similar(name,t)
                lev_d=jellyfish.levenshtein_distance(name,t)
                jaro_d=jellyfish.jaro_distance(name,t)
                dam_d=jellyfish.damerau_levenshtein_distance(name,t)
                name="_".join(name.split(" "))
                subs=int(name in t)+int(t in name)
                t="_".join(t.split(" "))
                try:sim=1 - spatial.distance.cosine(self.wtranse[name],self.wtranse[t])
                except Exception as e:sim=0
                cands+=[[name,t,wid,sim,pop,seq_sim,lev_d,jaro_d,dam_d,subs]]
        self.best_cands[name]=cands
        return cands

    def get_wikidata(self,name):
        try:return self.name2wikidata[name]
        except:
            wid=getWikipediaIdWikidataId("fr",name.capitalize())
            if wid=="NIL":wid=getWikipediaIdWikidataId("en",name.capitalize())
            self.name2wikidata[name]=wid
            return wid
    def dic_only(self,name):
        try:self.history[name]=self.exactLinker[name]
        except:self.history[name]="NIL"
    def wiki_only(self,name):
        self.history[name]=self.get_wikidata(name)#getWikipediaIdWikidataId("fr",name.capitalize())
    def dic_wiki(self,name):
        self.dic_only(name)
        if self.history[name]=="NIL":self.wiki_only(name)
    def wiki_dic(self,name):
        self.wiki_only(name)
        if self.history[name]=="NIL":self.dic_only(name)

    def cross_wiki(self,name):
        try:
            cands=self.croswiki[clean(name)]
            self.history[name]= cands[0][0]
        except:self.history[name]="NIL"
    def get_baseline(self,name):
        if self.args_dict.baseline=="dic_only":return self.dic_only(name)
        elif self.args_dict.baseline=="wiki_only":return self.wiki_only(name)
        elif self.args_dict.baseline=="wiki_dic":return self.wiki_dic(name)
        elif self.args_dict.baseline=="dic_wiki":return self.dic_wiki(name)
        else:print("Unknown baseline:{}".format(self.args_dict.baseline))
    def has_type(self,mid,typ):
        try:types=self.types[mid]
        except:
            try:
                types=get_entity(mid)
                self.types[mid]=set(types)
            except:return 0
        try:htyp=self.htype2wtype[typ]
        except:return 0
        return int(htyp in self.types[mid])
    def link_entity(self,name,typ):
        if typ=="time":return "NIL"
        try:
            mid=self.history[name]
            return mid
        except:pass
        candidates=self.best_generation(name.lower())
        tscore=0
        tmid="NIL"
        for eid in range(len(candidates)):
            mid,sim,pop=candidates[eid]
            if not self.has_type(mid,typ):continue
            if tscore<pop:
                tscore,tmid=pop,mid
        #if tscore<0.5:tmid="NIL"
        self.history[name]=tmid
        return tmid
    def features_cands(self,cands,name,typ,min_c=-1):
        try:return self.features[name]
        except:pass
        features_c=[]
        if min_c>0:nc=min(min_c,len(cands))
        else:nc=len(cands)
        for eid in range(nc):
            features=cands[eid]
            cmid=features[2]
            sametype=self.has_type(cmid,typ)
            if not sametype:continue
            features=features[3:]+[sametype]
            x=np.array(features)
            if self.args_dict.features!="":
                features=list(map(int,self.args_dict.features.split(",")))
                x=np.array([x[i] for i in features])
            features_c+=[x]
        self.features[name]=np.array(features_c)
        return np.array(features_c)
    def _link(self,name,typ):
        #get candidate entities
        cands=self.generate_candidate(name)
        #get the features
        x_test=self.features_cands(cands,name,typ,min_c=10)
        if not len(x_test):
            self.history[name]="NIL"
            return
        label,p=self.classifier.predict(x_test)
        p0,p1=p[:,0],p[:,1]
        if sum(label):#one of them is 1 at least
            best_c=np.argmax(p1)#best candidate from the classifier
            # if p1[best_c]>0.5:mid=cands[best_c][2]
            # else:mid="NIL"
            mid=cands[best_c][2]
        else:
            best_c=np.argmin(p0)#farest from non candidates
            mid=cands[best_c][2]
            # if p1[best_c]<0.5: mid=cands[best_c][2]
            # else:mid="NIL"
        self.history[name]=mid
      def _linktopk(self,name,typ):
        #get candidate entities
        cands=self.generate_candidate(name)
        #get the features
        x_test=self.features_cands(cands,name,typ,min_c=10)
        #get the qids
        qids=[x[2]for x in cands]
        if not len(x_test):
            self.history[name]="NIL"
            return
        label,p=self.classifier.predict(x_test)
        p0,p1=p[:,0],p[:,1]
        best0=[x for _,x in sorted(zip(p0,qids),key=lambda pair: pair[0],reverse=True)]
        best1=[x for _,x in sorted(zip(p0,qids),key=lambda pair: pair[0])]
        if sum(label):qids=best1[:6]
        else:qids=best0[:6]
        self.history[name]="|".join(qids)
       
    def link_name(self,name,typ):
        tic=time.time()
        try:return self.history[name]
        except:
            tic=time.time()
            self.get_baseline(name)
            toc=time.time()-tic
            #logger.info("baseline time {}".format(toc))
            # if self.substringLink and self.history[name]=="NIL":
            #     for hm in self.history:
            #         if hm in name or name in hm:self.history[name]=self.history[hm]
            tic=time.time()
            if self.history[name]=="NIL":self._link(name,typ)
            toc=time.time()-tic
            #logger.info("link classifier time {}".format(toc))
            if self.history[name]=="NIL":self.cross_wiki(name)
            mid=self.history[name]
        #if typ=="time":self.history[name]="NIL"
        toc=time.time()-tic
        #logger.info("link name time {}".format(toc))
        return self.history[name]
    def link_doc(self,tmpdoc,docid):
        tokid=0
        tokens=tmpdoc["lis_tokens"]
        mentions=0
        # try:
        #     _=self.mentions[docid]
        #     tokid=len(tokens)
        # except:self.mentions[docid]=1
        while tokid< len(tokens):
            token=tokens[tokid]
            
            name,typ,ids="","",""
            while  token["NE-COARSE-LIT"]!="O":
                #We have a mention not a comment
                if token["NE-COARSE-LIT"].startswith("B-") or token["NE-COARSE-LIT"].startswith("I-"):
                    if len(name)==0:name+=token["TOKEN"]+" "*(1-int(token['MISC']=="NoSpaceAfter"))
                    else:name+=token["TOKEN"]+" "*(1-int(token['MISC']=="NoSpaceAfter"))
                    if not len(typ):typ=token["NE-COARSE-LIT"][2:].lower()
                    if len(ids):ids+=";"+str(tokid)
                    else:ids=str(tokid)
                tokid+=1
                if tokid>=len(tokens):break
                token=tokens[tokid]
            if len(name):
                name=name.lower()
                try:self.mentions[(name,typ,docid)]+=";"+ids
                except:self.mentions[(name,typ,docid)]=ids
            # xname=token["TOKEN"].lower()
            # try:tmpdoc["lis_tokens"][idx]["NEL-LIT"]=self.name2wikidata[xname]#self.exactLinker[xname]
            # except:pass
            tokid+=1
        ######## We are linking the mention name! ###
        for mention in self.mentions:
            mentions+=1
            try:name,typ,did=mention
            except:continue
            
            if did!=docid:continue
            ids=self.mentions[mention]
            ids=list(map(int,ids.split(";")))
            #if len(ids)>5:logger.info("IDS:{} for {}".format(ids,name))
            #name=clean(name)
            mid=self.link_name(name,typ)
            tic=time.time()
            for idx in ids:
                try:
                   #t=tmpdoc["lis_tokens"][idx]["NEL-LIT"]
                   tmpdoc["lis_tokens"][idx]["NEL-LIT"]=mid #put the predicted entity
                except Exception as e:print("index problem",ids,name,e);exit()
            toc=time.time()-tic
            #logger.info("affect mids time {}".format(toc))
        #logger.info("Doc got:{} mentions".format(mentions))
        return tmpdoc,mentions
    def get_train(self):
        name="data/bigFiles/"
        if "15G" in self.embed_path:name+="15G."
        if "model-skipgram-300minc20-ws5-maxn-6.bin" in self.embed_path:name+="fasttext."
        try:
            X_train,y_train=np.load(self.path+name+"trainx.type.fr.npy"),np.load(self.path+name+"trainy.type.fr.npy")
        except:
            print("can't load files for train...")
            X_train,y_train=self.get_data(self.train_file)
        positive=sum(y_train)
        samples=len(y_train)
        print("train size:{}, positive examples:{} and negative examples:{}".format(samples,positive,samples-positive))
        if self.args_dict.features!="":
            features=list(map(int,self.args_dict.features.split(",")))
            X_train=np.array([np.array([x[i] for i in features])for x in X_train])
        return X_train,y_train
    def reset_nel(self,ccr):
        doci=0
        cr=ccr
        for doc in tqdm.tqdm(ccr):
            for token in range(len(doc["lis_tokens"])):
                doc["lis_tokens"][token]["NEL-LIT"]="NIL"
            cr.json[doci]=doc
            doci+=1
        return cr

    def link_conll(self,cr):
        
        doci=0
        tic=time.time()
        X_train,y_train=self.get_train()
        self.classifier=Classifier(X_train,y_train,self.args_dict)
        toc=time.time()-tic
        logger.info("Training time:{:0.3f}s".format(toc))
        tic=time.time()
        corpus_mentions=0
        cr=self.reset_nel(cr)
        ccr=cr.json
        for doc in tqdm.tqdm(ccr):
            cr.json[doci],mentions=self.link_doc(doc,doci)
            corpus_mentions+=mentions
            doci+=1
        toc=time.time()-tic
        logger.info("Prediction time:{:0.3f}s".format(toc))
        logger.info("Avg mention per doc:{:0.3f} mentions".format(corpus_mentions/doci))
        #self.dev_file_conll=cr
        try:
            cr.to_conll(self.output)
        except Exception as e:
            warnings.warn(f"""couldn't create {self.output} : {e}""")
        #print(self.classifier.model.coef_)


    def get_data(self,filename,min_c=10):
        print("building files to train the classifier")
        ccr=ConllReader(filename).json
        X_train,y_train=[],[]
        for doc in tqdm.tqdm(ccr):
            name=""
            tokid=0
            tokens=doc["lis_tokens"]
            pbar = tqdm.tqdm(total=len(tokens))
            while tokid< len(tokens):
                token=tokens[tokid]
                name,typ,mid="","",None
                while  token["NE-COARSE-LIT"]!="O":
                    #We have a mention not a comment
                    if token["NE-COARSE-LIT"].startswith("B-") or token["NE-COARSE-LIT"].startswith("I-"):
                        if len(name)==0:name+=token["TOKEN"]+" "*(1-int(token['MISC']=="NoSpaceAfter"))
                        else:name+=token["TOKEN"]+" "*(1-int(token['MISC']=="NoSpaceAfter"))
                        if not len(typ):
                            typ=token["NE-COARSE-LIT"][2:].lower()
                            mid=token["NEL-LIT"]
                    tokid+=1
                    if tokid>=len(tokens):break
                    token=tokens[tokid]
                    pbar.update(1)
                if len(name):
                    name=name.lower()
                    if len(typ)==0:print("type empty",name,typ);exit()
                    cands=self.generate_candidate(name)
                    co=0
                    for eid in range(len(cands)):
                        if co>min_c:break
                        features=cands[eid]
                        if len(features)!=10:
                            print(" Bad features shape",features);exit()
                        cmid=features[2]
                        sametype=self.has_type(cmid,typ)
                        if not sametype:continue
                        features=features[3:]+[sametype]
                        co+=1
                        X_train+=[np.array(features)]
                        y_train+=[np.array([int(cmid==mid)])]
                tokid+=1
                pbar.update(1)
            pbar.close()
        name="data/bigFiles/"
        if "15G" in self.embed_path:name+="15G."
        if "model-skipgram-300minc20-ws5-maxn-6.bin" in self.embed_path:name+="fasttext."
        np.save(self.path+name+"trainx.type.fr",np.array(X_train))
        np.save(self.path+name+"trainy.type.fr",np.array(y_train))
        self.save()
        return np.array(X_train),np.array(y_train)
    def get_mentions(self,tokens):
        tokid,mentions=0,[]
        while tokid< len(tokens):
            token=tokens[tokid]
            name,typ="",""
            while  token["NE-COARSE-LIT"]!="O":
                #We have a mention not a comment
                if token["NE-COARSE-LIT"].startswith("B-") or token["NE-COARSE-LIT"].startswith("I-"):
                    if len(name)==0:name+=token["TOKEN"]+" "*(1-int(token['MISC']=="NoSpaceAfter"))
                    else:name+=token["TOKEN"]+" "*(1-int(token['MISC']=="NoSpaceAfter"))
                    if not len(typ):
                        typ=token["NE-COARSE-LIT"][2:].lower()
                        mid=token["NEL-LIT"]
                tokid+=1
                if tokid>=len(tokens):break
                token=tokens[tokid]
            if len(name):mentions+=[(name.lower(),typ,mid)]
            tokid+=1
        return mentions
    def save(self):
        np.save(self.path+"data/bigFiles/cands.fr.npy",self.cands)
        np.save(self.path+"data/bigFiles/best_cands.fr.npy",self.best_cands)
        np.save(self.path+"data/bigFiles/types.fr.npy",self.types)
        np.save(self.path+"data/bigFiles/name2wikidata.fr.npy",self.name2wikidata)
        np.save(self.path+"data/bigFiles/history.{}.fr.npy".format(self.test_alis),self.history)
        np.save(self.path+"data/bigFiles/mentions.{}.fr.npy".format(self.test_alis),self.mentions)
        
    def load(self):
        # try:self.croswiki=np.load(self.path+"data/bigFiles/croswikidata.npy",allow_pickle=True).item()
        # except:self.croswiki={}
        try:self.cands=np.load(self.path+"data/bigFiles/cands.fr.npy",allow_pickle=True,encoding="latin1").item()
        except:self.cands={}
        try:self.types=np.load(self.path+"data/bigFiles/types.fr.npy",allow_pickle=True,encoding="latin1").item()
        except:self.types={}
        try:self.best_cands=np.load(self.path+"data/bigFiles/best_cands.fr.npy",allow_pickle=True,encoding="latin1").item()
        except:self.best_cands={}
        try:self.crossfr=np.load(self.path+"data/bigFiles/cands_cm.fr.npy",allow_pickle=True,encoding="latin1").item()
        except:self.crossfr={}
        try:self.name2wikidata=np.load(self.path+"data/bigFiles/name2wikidata.fr.npy",allow_pickle=True,encoding="latin1").item()
        except:self.name2wikidata={}
        self.cm=np.load(self.path+"data/bigFiles/cm.fr.npy",allow_pickle=True,encoding="latin1").item()
        self.exactLinker=np.load(self.path+"data/bigFiles/exactMatcher.npy",allow_pickle=True,encoding="latin1").item()
        try:self.wtranse=KeyedVectors.load_word2vec_format(self.args_dict.embed_file, binary=True,unicode_errors="ignore")
        except:self.wtranse = FastText.load_fasttext_format(self.args_dict.embed_file)
        try:self.wsrm=np.load(self.path+"data/bigFiles/wsrm-wikidata.npy",allow_pickle=True,encoding="latin1").item()
        except:self.wsrm={}
        try:
            self.qid2num=np.load(self.path+"data/bigFiles/wikidata_IDS.npy",allow_pickle=True,encoding="latin1").item()
            self.num2qid={self.qid2num[key]:key for key in self.qid2num}
        except:self.qid2num=self.num2qid={}
        try:self.history=np.load(self.path+"data/bigFiles/history.{}.fr.npy".format(self.test_alis),allow_pickle=True,encoding="latin1").item()
        except:self.history={}
        self.history={}
        try:self.mentions=np.load(self.path+"data/bigFiles/mentions.{}.fr.npy".format(self.test_alis),allow_pickle=True,encoding="latin1").item()
        except:self.mentions={}


        logging.info("Found:{} cands, {} types, {} cm, {} exact match".format(len(self.cands),len(self.types),len(self.cm),len(self.exactLinker)))

    def get_entity_folks(self,qid,threshold=0.5):
        '''Return the top linked entities to a given entity according to WSRM:
        normalized sum of commun links between two entities'''
        folks=[]
        for key in tqdm.tqdm(self.wsrm):
            if self.qid2num[qid]==key[0] and self.wsrm[key]>threshold:folks+=[(self.num2qid[key[1]],self.wsrm[key])]
            elif self.qid2num[qid]==key[1] and self.wsrm[key]>threshold:folks+=[(self.num2qid[key[0]],self.wsrm[key])]
        folks=sorted(folks,key=lambda x:x[-1],reverse=True)
        return get_names(folks)

    def get_name_and_mention_from_wikidata(self, Qid):
        '''not all Q-items that are in the dev file are in self.dict_Q_mention
        that function gets it from wikidata'''
        ua = UserAgent()
        headers = {'User-Agent': ua.random,}
        url = 'https://www.wikidata.org/wiki/' + Qid
        try:
            r = requests.get(url, headers=headers)
            soup = BeautifulSoup(r.text, 'html.parser')
            name = soup.find('span', class_="wikibase-title-label").text
            full_mention = soup.find(lang='fr').text
            return {'name' : name.strip().replace(' ', '_').lower(),
            'full_mention': full_mention.strip().replace(' ', '_').lower()}
        except Exception as e:
            print(e)
            return {'name' : 'None', 'full_mention': 'None'}


    def create_dict_folks(self, out_dict="dict_folks.pkl"):
        """create and save the dictionnary of folks bases on Qid
        eg: {'Q23' : ['Category:Alumni by university or college in Switzerland', 'Hildebrand affair', 'Denise Gilliand', 'Didier Berthod', 'Rachel Seydoux', '1943 Swiss federal election', 'Eroica Spiess', 'Christophe Gesseney', 'Federal popular initiative "against mass immigration"', ... '"""
        touch(out_dict)
        folks = {}
        for i, doc in enumerate(self.dev_file_conll):
            print(f'{i}\tof\t{len(self.dev_file_conll)}\tdocuments', flush=True)
            for tok in doc["lis_tokens"]:
                if tok["NEL-LIT"].startswith("Q"):
                    try:
                        folks[tok["NEL-LIT"]] = list(self.get_entity_folks(tok["NEL-LIT"]))
                    except Exception as e:
                        warnings.warn(f'error with {tok["NEL-LIT"]} - {e}')
                        folks[tok["NEL-LIT"]] = []
        # deleting the 'categories:'
        for val in folks.values():
            for val_ in val:
                val_ = val_.split(":")[-1]
        try:
            pickle.dump(folks, open(out_dict, 'bw'))
            print(f'outfile:\t{out_dict}')
        except Exception as e:
            print(e)
        return folks

    def load_dict_folks(self, dict_="dict_folks.pkl"):
        '''load the dict_folks to the dict_folks attribute'''
        self.dict_folks = pickle.load(open(dict_, 'br'))

    def construct_dict_Q_mentions(self):
        '''associate a dictionnay of Q, mentions to the self.dict_Q_mention
        {'Q2495191': {'name': 'saint-pétersbourg',
        'full_mention': 'novodevichy_cemetery'},
        'Q2995349': {'name': 'saint-pétersbourg',
        'full_mention': 'consulate-general_of_france_in_saint_petersburg'},
        'Q63314650': {'name': 'saint-pétersbourg',
        '''
        for cand in self.cands.values():
            if not cand:
                continue
            for lis in cand:
                assert lis[2].startswith('Q')
                self.dict_Q_mention[lis[2]] = {'name' : lis[0].strip('_'), 'full_mention': lis[1].split(':')[-1].strip('_')}
        for doc in tqdm.tqdm(self.dev_file_conll):
            for tok in doc['lis_tokens']:
                if tok['NEL-LIT'].startswith('Q'):
                    try:
                        _=(tok['TOKEN'], tok['NE-COARSE-LIT'], tok['NEL-LIT'], self.dict_Q_mention[tok['NEL-LIT']])
                    except KeyError:
                        self.dict_Q_mention[tok['NEL-LIT']] = self.get_name_and_mention_from_wikidata(tok['NEL-LIT'])

    def rule_look_for_ents_with_dict(self):
        """use the self.dict_folks dictionnary to look for
        entities that could have been missed while performing the NER
        'Q34': ['Savolax and Kymmenegård County',
        'Jämthund',
        'Plop!',
        'Christoffer Eriksson',
        'Camilla Lindholm',"""
        for doc, doc_offi in tqdm.tqdm(zip(self.dev_file_conll, self.original_dev_file_conll)):  # XXX
            ls_folks = []
            for tok in doc["lis_tokens"]:
                if tok["NEL-LIT"].startswith("Q"):
                    try:
                        ls_folks += self.dict_folks[tok["NEL-LIT"]]
                    except KeyError:
                        continue
                    ls_folks = list(set(ls_folks))
                    for folk in ls_folks:
                        # if len(re.split(r'\W+', folk)) == 1:
                            # folk_tok = folk
                        for folk_tok in re.split(r"\W+", folk):
                            for tok, tok_offi in zip(doc["lis_tokens"], doc_offi['lis_tokens']):
                                if (
                                    unidecode.unidecode(folk_tok.lower()) == unidecode.unidecode(tok["TOKEN"].lower())
                                    and folk_tok.lower() not in STOPWORDS
                                    and tok["NE-COARSE-LIT"] == "O"
                                    and len(folk_tok) > 3
                                ):
                                    # print(tok['TOKEN'], folk_tok, tok['NE-COARSE-LIT'], tok_offi['NE-COARSE-LIT'], folk, doc['document_id'], sep='\t')
                                    tok['NE-COARSE-LIT'] = 'B-' + type_




from params import get_parser
def main():
    # Load parameters

    parser = get_parser()
    args_dict, unknown = parser.parse_known_args()

    opts = vars(args_dict)
    print('------------ Options -------------')
    for k, v in sorted(opts.items()):
        print('%s: %s' % (str(k), str(v)))
    print('-----------------------------------')

    np.random.seed(args_dict.seed)
    linker=Linker(args_dict)
    #X,y=linker.get_data(linker.train_file)
    try:
        dev_file_conll = ConllReader(os.path.join(args_dict.path, args_dict.dev))
        linker.link_conll(dev_file_conll)
        linker.save()
    except Exception as e:
        linker.save()
        print(e)

if __name__ == "__main__":
    #make_crosswiki("../data/bigFiles/")
    main()
