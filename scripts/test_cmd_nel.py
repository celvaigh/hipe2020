import requests, json, sys
import tqdm
import string
from bs4 import BeautifulSoup
from fake_useragent import UserAgent


def generate_candidate(name):
    ua = UserAgent()
    headers = {
        "User-Agent": ua.random,
    }
    url = "https://www.wikidata.org/w/index.php?search=&search={}&title=Special%3ASearch&go=Go&ns0=1&ns120=1".format(
        name
    )
    r = requests.get(url, headers=headers)
    # print(r.text)
    soup = BeautifulSoup(r.text, "html.parser")
    # itemlinks=soup.find_all('span', class_="wb-itemlink-id")
    links = soup.find_all("a")
    cands = []
    for link in tqdm.tqdm(links):
        a = link.get("href")
        if a != None and a.startswith("/wiki/Q"):
            title = link.get("title")
            if "|" in title:
                x = title.split(" | ")
                t = x[0]
            else:
                t = title
            t = "".join([x for x in t if x in string.printable])
            t, name = t.lower(), name.lower()
            cands += [[name, t, a.replace("/wiki/", "")]]
    return cands


def updatenel(dic, other):
    others = [other]
    for m in tqdm(dic):
        try:
            if other in m or m in other:
                others += [m]
        except:
            pass
    return list(set(others))


from pipeline import *

parser = get_parser()
args_dict, unknown = parser.parse_known_args()
args_dict.path = "/mnt/nfs/nas4/cicoda_tmp/cicoda_tmp/HIPE/hipe2020/"
args_dict.features = "0,1,2,3,4,5,6"
args_dict.embed_file = "/mnt/nfs/nas4/cicoda_tmp/cicoda_tmp/word2vec/trunk/models/skipgram.s100.w5.enwikianchor1G.bin"
pipe = Pipeline(args_dict)
pipe.train_predict()
pipe.link_conll(pipe.dev_file_conll)


def timenil(mentions, history):
    for mention in tqdm(mentions):
        try:
            name, typ, did = mention
        except:
            continue
        if typ == "time":
            self.history[name] = "NIL"
        history[name] = "NIL"
