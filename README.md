# HIPE2020

IRISA system for [HIPE - Shared Task](https://impresso.github.io/CLEF-HIPE-2020//tasks.html)

The scorer is available on [github](https://github.com/impresso/CLEF-HIPE-2020-scorer) or in the [scripts directory](./scripts)

# Baseline

```python
on data v0.9 fr:
['NE-COARSE-LIT']['ALL']['strict']['F1_micro'] : 0.623134328358209
['NE-COARSE-LIT']['ALL']['ent_type']['F1_micro'] : 0.7500000000000001

on data v1.0 fr:

['NE-COARSE-LIT']['ALL']['strict']['F1_micro']     : 0.6221672847136384
['NE-COARSE-LIT']['ALL']['ent_type']['F1_micro']   : 0.735063864853729
['NE-COARSE-METO']['ALL']['strict']['F1_micro']    : 0.3308270676691729
['NE-COARSE-METO']['ALL']['ent_type']['F1_micro']  : 0.3308270676691729



perso (best so far):
['NE-COARSE-LIT']['ALL']['strict']['F1_micro']    :
['NE-COARSE-LIT']['ALL']['ent_type']['F1_micro']  :
['NE-COARSE-METO']['ALL']['strict']['F1_micro']   :
['NE-COARSE-METO']['ALL']['ent_type']['F1_micro'] :

0.6965845909451946
0.8038125496425735
0.39999999999999997
0.42580645161290326

w/ algorithm="lbfgs", c1=0.2, c2=0.1, max_iterations=199, all_possible_transitions=True

even better:

["NE-COARSE-LIT"]["ALL"]["strict"]["F1_micro"]	0.7071428571428572
["NE-COARSE-LIT"]["ALL"]["ent_type"]["F1_micro"]	0.8182539682539681


w/ {'algorithm': 'lbfgs', 'c1': 0.17978644989356052, 'c2': 0.05506289813560372, 'min_freq': 0.6682436776025411, 'max_iterations': 192, 'all_possible_transitions': False, 'num_memories': 4, 'epsilon': 1e-05, 'period': None, 'delta': 0.0001, 'linesearch': None, 'max_linesearch': None, 'all_possible_states': False}

w/ same parameters + FT :
# fr-model-skipgram-300minc20-ws5-maxn-0.bin, model[word][:100]
["NE-COARSE-LIT"]["ALL"]["strict"]["F1_micro"]	0.7158230370665605
["NE-COARSE-LIT"]["ALL"]["ent_type"]["F1_micro"]	0.82104424073336



```


# NEL simple system:
```python
  using the prediction of ner
  ["NEL-LIT"]["ALL"]["strict"]["F1_micro"]:0.4547353760445682
  ["NEL-LIT"]["ALL"]["ent_type"]["F1_micro"]:0.5041782729805013
  using the prediction the ground truth
  ["NEL-LIT"]["ALL"]["strict"]["F1_micro"]:0.39019112874143524
  ["NEL-LIT"]["ALL"]["ent_type"]["F1_micro"]:0.4060584204832311
```

# NEL with dic+wikidata search+classifier (One by One):

```python
  using the prediction of ner
  nil th_0.5
  ["NEL-LIT"]["ALL"]["strict"]["F1_micro"]:0.42594075260208164
  ["NEL-LIT"]["ALL"]["ent_type"]["F1_micro"]:0.4819855884707766

  nil th_0.3
  strict|F1_micro:0.4267413931144916 ent_type|F1_micro:0.48278622898318657
```
# NEL irisa-ner-crf-nelv2-topk-5_bundle2_fr_1_nel_results.tsv:
```python
                                System	                Evaluation	    Label	P	    R	    F1	  F1_std	P_std	R_std	TP	FP	FN
irisa-ner-crf-nelv2-topk-5_bundle2_fr_1	NEL-LIT-micro-fuzzy-best@1	    ALL	  0.537	0.597	0.565				               797	687	539
irisa-ner-crf-nelv2-topk-5_bundle2_fr_1	NEL-LIT-macro_doc-fuzzy-best@1	ALL	  0.541	0.615	0.574	0.136	0.123	0.16			
irisa-ner-crf-nelv2-topk-5_bundle2_fr_1	NEL-METO-micro-fuzzy-best@1	    ALL	  0.045	0.671	0.085				               53	1118	26
irisa-ner-crf-nelv2-topk-5_bundle2_fr_1	NEL-METO-macro_doc-fuzzy-best@1	ALL	  0.04	0.669	0.189	0.139	0.077	0.323
```
# NER - NEL - NER:
```
{'results_2nd_turn': (0.680577849117175, 0.7825040128410915),
 'results_initia': (0.6850740296118447, 0.7875150060024009)}

```

# NER - NEL - NER:
En applicant, NER, NEL, puis correction des types du NER via les types du NEL:
```
{'results_1_initia': (0.7158230370665605, 0.82104424073336),
 'results_2nd_turn': (0.3985651654045437, 0.48943802311677964)}

```


# Avec les copains:
### en utilisant unidecode(chaine.lower())

| Threshold          | ["NE-COARSE-LIT"]["ALL"]["ent_type"]["F1_micro"] | ["NE-COARSE-LIT"]["ALL"]["strict"]["F1_micro"] |
|-------------------|--------------------------------------------------|------------------------------------------------|
| dict_folks_th_0.1 | 0.790733004177744                                | 0.685909608811242                              |
| dict_folks_th_0.2 | 0.7735217552993678                               | 0.6731126812941615                             |
| dict_folks_th_0.5 | 0.7829722538958571                               | 0.6826301786393008                             |
| dict_folks_th_0.8 | 0.7933947772657449                               | 0.6920122887864822                             |
| dict_folks_th_0.9 | 0.792481779823552                                | 0.6912159570387418                             |


### en utilisant jellyfish.damerau_levenshtein_distance(unidecode(folk_tok.lower() <2

| Threshold             | ["NE-COARSE-LIT"]["ALL"]["ent_type"]["F1_micro"] | ["NE-COARSE-LIT"]["ALL"]["strict"]["F1_micro"] |
|-----------------------|--------------------------------------------------|------------------------------------------------|
| dict_folks_th_0.1 | 0.5698630136986301                               | 0.49260273972602747                            |
| dict_folks_th_0.2 | 0.49819494584837554                              | 0.4322503008423586                             |
| dict_folks_th_0.5 | 0.6109645265318089                               | 0.5265318088537085                             |
| dict_folks_th_0.6 |  0.6087212728344137 | 0.5315262227460225 |
| dict_folks_th_0.7  |  0.6416149068322982 | 0.5602484472049689 |
| dict_folks_th_0.8 | 0.6526381909547738                               | 0.5640703517587939                             |
| dict_folks_th_0.9 | 0.6457426973275326                               | 0.5581106277190803                             |




# Documentation
## ConllReader Objects

```
>>> from hipe_utils import ConllReader
>>> cr=ConllReader('../data/training-v1.0/fr/HIPE-data-v1.0-train-fr.tsv')
>>> cr
['../data/training-v1.0/fr/HIPE-data-v1.0-train-fr.tsv']
	6738610 characters
	156844 tokens
	175881 lines
	151 documents
	1 files
>>> for doc in cr:
...     print(doc['document_id'])
...
EXP-1888-01-09-a-i0035
EXP-1908-01-21-a-i0053
EXP-1908-01-31-a-i0040
EXP-1908-05-15-a-i0085
...

```
chaque document est sous forme d'un dictionnaire accessible via `cr.json`:
```
>>> cr.json[0].keys()
dict_keys(['lis_tokens', 'language', 'newspaper', 'date', 'document_id'])

```

On peut recuperer les tokens via:

```
>>> pprint(cr.json[0]['lis_tokens'][:10])
[{'IS_WORD': False,
  'MISC': 'NA',
  'NE-COARSE-LIT': 'NA',
  'NE-COARSE-METO': 'NA',
  'NE-FINE-COMP': 'NA',
  'NE-FINE-LIT': 'NA',
  'NE-FINE-METO': 'NA',
  'NE-NESTED': 'NA',
  'NEL-LIT': 'NA',
  'NEL-METO': 'NA',
  'POSITION': 0,
  'TOKEN': '# segment_iiif_link = '
           'https://iiif.dhlab.epfl.ch/iiif_impresso/EXP-1888-01-09-a-p0004/495,727,228,51/full/0/default.jpg'},
 {'IS_WORD': True,
  'MISC': '_',
  'NE-COARSE-LIT': 'O',
  'NE-COARSE-METO': 'O',
  'NE-FINE-COMP': 'O',
  'NE-FINE-LIT': 'O',
  'NE-FINE-METO': 'O',
  'NE-NESTED': 'O',
  'NEL-LIT': '_',
  'NEL-METO': '_',
  'POSITION': 0,
  'TOKEN': 'NOUVELLES'},
 {'IS_WORD': True,
  'MISC': 'EndOfLine',
  'NE-COARSE-LIT': 'O',
  'NE-COARSE-METO': 'O',
  'NE-FINE-COMP': 'O',
  'NE-FINE-LIT': 'O',
  'NE-FINE-METO': 'O',
  'NE-NESTED': 'O',
  'NEL-LIT': '_',
  'NEL-METO': '_',
  'POSITION': 1,
  'TOKEN': 'SUISSES'},

```

 Note: la clef `'IS_WORD'` a ete rajoutee.
 Les objects `ConllReader` disposent d'autres methodes:

```
>>> cr.to_text(outname) # enregistre le texte du conll dans le fichier
>>> print(cr.to_text()[:1000])
# language = fr
# newspaper = EXP
# date = 1888-01-09
# document_id = EXP-1888-01-09-a-i0035
NOUVELLES SUISSES
— En 1887, la Société suisse du Grutli
s'est accrue de 40 sections ; l'association
compte actuellement 12,000 membres.
Environ 30,000 fr. ont été versés dans
la caisse créée pour subventionner les
grèves.
Ecoles de fonctionnaires. — On parle
de fonder deux facultés de sciences
politiques, l'une à Berne, l'autre à Genève
ou à Lausanne, afin de donner ainsi aux
```

 Pour convertir un objet en fichier conll:

```
 >>> cr.to_conll(outname) # enregistre le conll dans le fichier
 >>> print(cr.to_conll()[:1000])
TOKEN	NE-COARSE-LIT	NE-COARSE-METO	NE-FINE-LIT	NE-FINE-METO	NE-FINE-COMP	NE-NESTED	NEL-LIT	NEL-METO	MISC
# language = fr
# newspaper = EXP
# date = 1888-01-09
# document_id = EXP-1888-01-09-a-i0035
# segment_iiif_link = https://iiif.dhlab.epfl.ch/iiif_impresso/EXP-1888-01-09-a-p0004/495,727,228,51/full/0/default.jpg
NOUVELLES	O	O	O	O	O	O	_	_	_
SUISSES	O	O	O	O	O	O	_	_	EndOfLine
# segment_iiif_link = https://iiif.dhlab.epfl.ch/iiif_impresso/EXP-1888-01-09-a-p0004/463,784,311,43/full/0/default.jpg
—	O	O	O	O	O	O	_	_	_
En	B-time	O	B-time.date.abs	O	O	O	NIL	_	_
1887	I-time	O	I-time.date.abs	O	O	O	NIL	_	NoSpaceAfter
,	O	O	O	O	O	O	_	_	_
la	O	O	O	O	O	O	_	_	_
Société	B-org	O	B-org.ent	O	O	O	Q683672	_	_
suisse	I-org	O	I-org.ent	O	O	O	Q683672	_	_
du	I-org	O	I-org.ent	O	O	O	Q683672	_	_
Grutli	I-org	O	I-org.ent	O	O	B-loc.phys.geo	Q683672	_	EndOfLine
# segment_iiif_link = https://iiif.dhlab.epfl.ch/iiif_impresso/EXP-1888-01-09-a-p0004/442,806,331,43/full/0/default.jpg
s	O	O	O	O	O	O	_	_	NoSpaceAfter

```


## Linker Objects
Les objets `Linker` s'initialisent via un dictionnaire de type parser:

```
>>> from NELv2 import Linker
>>> from params import get_parser

>>> parser = get_parser()
>>> args_dict, unknown = parser.parse_known_args()
>>> link = Linker(args_dict)

```
Pour recuperer la lister des entites candidates en utilisant les resultats de recher wikipedia:
```
>>> res = link.generate_candidates_from_wikipedia('jean', lang='fr')
>>> res
[{'Q_id': 'Q76',
  'title': 'Barack Obama',
  'url': 'https://fr.wikipedia.org//wiki/Barack_Obama',
  'wikidata_URL': 'https://www.wikidata.org/wiki/Q76'},
 {'Q_id': 'Q13133',
  'title': 'Michelle Obama',
  'url': 'https://fr.wikipedia.org//wiki/Michelle_Obama',
  'wikidata_URL': 'https://www.wikidata.org/wiki/Q13133'},
 {'Q_id': 'Q1379733',
  'title': 'Présidence de Barack Obama',
  'url': 'https://fr.wikipedia.org//wiki/Pr%C3%A9sidence_de_Barack_Obama',
  'wikidata_URL': 'https://www.wikidata.org/wiki/Q1379733'},
...

```

## Pipeline Objects

Les objets `Pipeline` heritent des objects `Hipe_CRF` et des objets `Linker`. Ils s'initialisent via un dictionnaire de type parser.

```
>>> pipe = Pipeline(args_dict)
```
### attributs:
Pipeline.types: dictionnaire Id-types:
```
>>> pipe.types["Q76"]
	{'agent',
	 'concrete object',
	 'consumer',
	 'creature',
	 'heterotroph',
	 'human',
	 'individual',
	 ...
```
 Pipeline.generate_candidate: retourne une liste de candidats (et l'associe a Pipeline.cands
```
 >>> pipe.generate_candidate('Sarkozy')
 	 [['sarkozy',
	  'nicolas_sarkozy',
	  'Q329',
	  0.7514546513557434,
	  0.9790209790209791,
	  0.6363636363636364,
	  8,
	  0.4317460317460317,
	  8,
	  1],
	 ['sarkozy',
	  'sarkozy',
	  'Q1638273',
	  1.0,
	  0.013986013986013986,
	  1.0,
	  0,
	  1.0,
	  0,
	  2],
	 ['sarkozy',
	  'sarkozy',
	  ...
```


 dictionnaires des types:
```
 >>>pipe.wtype2htype
          {'geographic location': 'loc',
	 'organization': 'org',
	 'human': 'pers',
	 'product': 'prod'}

>>> pipe.htype2wtype
  {'loc': 'geographic location',
	 'org': 'organization',
	 'pers': 'human',
	 'prod': 'product'}
```
pour construire le dictionnaire Q/mentions:
```
>>> pipe.construct_dict_Q_mentions() 
>>> pipe.dict_Q_mention
{ 'Q3401716': {'name': 'saint-pétersbourg',
  'full_mention': 'the_first_saint-petersburg_gymnasium'},
 'Q359185': {'name': 'saint-pétersbourg',
  'full_mention': 'admiralty_building_in_saint_petersburg'},
 'Q2727598': {'name': 'guerre_1914-1918',
  'full_mention': 'croix_de_guerre_19141918'},
 'Q691416': {'name': 'guerre_1914-1918',
  'full_mention': 'mdaille_commmorative_de_la_guerre_19141918'},
 ...}

```
pour retourner le label hipe associe a un Qid:
```
>>> pipe.get_hipe_label('Q33')
'loc'
```


# Resultats envoyes:

Bundle2 (NERC-coarse and NEL):

- irisa_bundle4_fr_1.tsv : --addOtherFiles
- irisa_bundle4_fr_2.tsv : --addOtherFiles + --addDevFile
- irisa_bundle4_fr_23tsv : --addOtherFiles + FT

Bundle4 (NERC-coarse): 



- 







